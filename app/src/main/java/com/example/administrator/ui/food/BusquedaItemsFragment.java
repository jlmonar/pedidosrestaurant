package com.example.administrator.ui.food;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.Utility;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.retrofit.response.Item;
import com.example.administrator.viewmodel.ItemViewModel;

import ru.nikartm.support.ImageBadgeView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BusquedaItemsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BusquedaItemsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BusquedaItemsFragment extends Fragment implements View.OnClickListener{
    private MyPlatoComidaRecyclerViewAdapter adaptadorItem;
    private RecyclerView recyclerViewPlatosComida;
    ItemViewModel itemViewModel;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private ImageButton btnSearchProduct;
    private EditText etSearchItem;
    private TextView tvVerTodosItems;

    private OnFragmentInteractionListener mListener;

    public BusquedaItemsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BusquedaItemsFragment.
     */
    public static BusquedaItemsFragment newInstance(String param1, String param2) {
        BusquedaItemsFragment fragment = new BusquedaItemsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_busqueda_items, container, false);

        //Evitar que teclado aparezca automaticamente al iniciar activity.
        btnSearchProduct = view.findViewById(R.id.btnSearchProduct);
        tvVerTodosItems = view.findViewById(R.id.tvVerTodosItems);

        etSearchItem = view.findViewById(R.id.etSearchItem);
        btnSearchProduct.setOnClickListener(this);
        tvVerTodosItems.setOnClickListener(this);

        recyclerViewPlatosComida = view.findViewById(R.id.recyclerViewPlatosComida);

        int numberOfColumns = Utility.calculateNoOfColumns(getContext(), 150);
        recyclerViewPlatosComida.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
        recyclerViewPlatosComida.setHasFixedSize(true);

        itemViewModel = ViewModelProviders.of(getActivity()).get(ItemViewModel.class);

        ImageBadgeView ibvShoppingCart = (ImageBadgeView)getActivity().findViewById(R.id.ibvShoppingCart);
        adaptadorItem = new MyPlatoComidaRecyclerViewAdapter(getActivity(), ibvShoppingCart);

        itemViewModel.itemPagedList.observe(getActivity(), new Observer<PagedList<Item>>() {
            @Override
            public void onChanged(PagedList<Item> items) {
                adaptadorItem.submitList(items);
            }
        });

        recyclerViewPlatosComida.setAdapter(adaptadorItem);

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSearchProduct: {
                //Se esconde el teclado
                InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                if (etSearchItem.getText().length() == 0) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Sin resultados.");
                    alert.setMessage("Escriba un texto de búsqueda antes de continuar.");
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    return;
                }

                itemViewModel.setItemPagedList(ClaseGlobal.GET_ITEMS_BY_NAME, etSearchItem.getText().toString(), "");
                itemViewModel.itemPagedList.observe(getActivity(), new Observer<PagedList<Item>>() {
                    @Override
                    public void onChanged(PagedList<Item> items) {
                        adaptadorItem.submitList(items);

                        /* Cada vez que hacemos una busqueda desde el servidor, desactivamos este observer
                         * para que no se vuelva a lanzar cada vez que se hace una busqueda,
                         * para ello invocamos el método removeObserver.
                         */
                        itemViewModel.itemPagedList.removeObserver(this);
                    }
                });

                break;
            }
            case R.id.tvVerTodosItems: {
                itemViewModel.setItemPagedList(ClaseGlobal.GET_ALL_ITEMS, "", "");
                itemViewModel.itemPagedList.observe(getActivity(), new Observer<PagedList<Item>>() {
                    @Override
                    public void onChanged(PagedList<Item> items) {
                        adaptadorItem.submitList(items);

                        /* Cada vez que hacemos una busqueda desde el servidor, desactivamos este observer
                         * para que no se vuelva a lanzar cada vez que se hace una busqueda,
                         * para ello invocamos el método removeObserver.
                         */
                        itemViewModel.itemPagedList.removeObserver(this);
                    }
                });
                break;
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <progressDialog>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();

        adaptadorItem.notifyDataSetChanged();
        recyclerViewPlatosComida.setAdapter(adaptadorItem);
    }
}
