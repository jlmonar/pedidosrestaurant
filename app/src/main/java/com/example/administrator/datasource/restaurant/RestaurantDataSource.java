package com.example.administrator.datasource.restaurant;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.retrofit.RetrofitClient;
import com.example.administrator.retrofit.response.ResponseRestaurants;
import com.example.administrator.retrofit.response.Restaurant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantDataSource extends PageKeyedDataSource<Integer, Restaurant> {
    public static final int PAGE_SIZE = 5;
    public static final int FIRST_PAGE = 1;
    private String retrofit_method = ClaseGlobal.GET_ALL_RESTAURANTS;
    private String texto_busqueda;

    public RestaurantDataSource() {
    }

    public RestaurantDataSource(String retrofit_method, String texto_busqueda) {
        this.retrofit_method = retrofit_method;
        this.texto_busqueda = texto_busqueda;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Restaurant> callback) {
        switch (retrofit_method) {
            case ClaseGlobal.GET_ALL_RESTAURANTS: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getAllRestaurants(PAGE_SIZE, FIRST_PAGE)
                        .enqueue(new Callback<ResponseRestaurants>() {
                            @Override
                            public void onResponse(Call<ResponseRestaurants> call, Response<ResponseRestaurants> response) {
                                if(response.body() != null) {
                                    callback.onResult(response.body().getData(), null, FIRST_PAGE + 1);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseRestaurants> call, Throwable t) {
                                t.printStackTrace();
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor  (load init). ", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_RESTAURANTS_BY_NAME: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getRestaurantsByName(texto_busqueda, PAGE_SIZE, FIRST_PAGE)
                        .enqueue(new Callback<ResponseRestaurants>() {
                            @Override
                            public void onResponse(Call<ResponseRestaurants> call, Response<ResponseRestaurants> response) {
                                if(response.body() != null) {
                                    callback.onResult(response.body().getData(), null, FIRST_PAGE + 1);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseRestaurants> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor  (load init).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
        }

    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Restaurant> callback) {
        switch (retrofit_method) {
            case ClaseGlobal.GET_ALL_RESTAURANTS: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getAllRestaurants(PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseRestaurants>() {
                            @Override
                            public void onResponse(Call<ResponseRestaurants> call, Response<ResponseRestaurants> response) {
                                if(response.body() != null) {
                                    Integer key = (params.key > 1) ? params.key - 1 : null;
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseRestaurants> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load before).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_RESTAURANTS_BY_NAME: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getRestaurantsByName(texto_busqueda, PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseRestaurants>() {
                            @Override
                            public void onResponse(Call<ResponseRestaurants> call, Response<ResponseRestaurants> response) {
                                if(response.body() != null) {
                                    Integer key = (params.key > 1) ? params.key - 1 : null;
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseRestaurants> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load before).", Toast.LENGTH_SHORT).show();
                            }
                        });
                 break;
            }
        }
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Restaurant> callback) {
        switch (retrofit_method) {
            case ClaseGlobal.GET_ALL_RESTAURANTS: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getAllRestaurants(PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseRestaurants>() {
                            @Override
                            public void onResponse(Call<ResponseRestaurants> call, Response<ResponseRestaurants> response) {
                                if(response.body() != null) {
                                    Integer key = null;

                                    if (response.body().getMeta().getPagination().getCurrentPage() != response.body().getMeta().getPagination().getTotalPages()) {
                                        key = params.key + 1;
                                    }
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseRestaurants> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load after).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_RESTAURANTS_BY_NAME: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getRestaurantsByName(texto_busqueda, PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseRestaurants>() {
                            @Override
                            public void onResponse(Call<ResponseRestaurants> call, Response<ResponseRestaurants> response) {
                                if(response.body() != null) {
                                    Integer key = null;

                                    if (response.body().getMeta().getPagination().getCurrentPage() != response.body().getMeta().getPagination().getTotalPages()) {
                                        key = params.key + 1;
                                    }
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseRestaurants> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load after).", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }
    }
}
