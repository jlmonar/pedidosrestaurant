package com.example.administrator.datasource.item;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.retrofit.response.Item;

public class ItemDataSourceFactory extends DataSource.Factory {
    private MutableLiveData<PageKeyedDataSource<Integer, Item>> itemLiveData = new MutableLiveData<>();
    private String retrofit_method = ClaseGlobal.GET_ALL_ITEMS;
    private String texto_busqueda = "";
    private String categoria = "";

    public ItemDataSourceFactory() {
    }

    public ItemDataSourceFactory(String retrofit_method, String texto_busqueda, String categoria) {
        this.retrofit_method = retrofit_method;
        this.texto_busqueda = texto_busqueda;
        this.categoria = categoria;
    }

    @Override
    public DataSource create() {
        ItemDataSource itemDataSource = new ItemDataSource(this.retrofit_method, this.texto_busqueda, this.categoria);
        itemLiveData.postValue(itemDataSource);

        return itemDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, Item>> getItemLiveData() {
        return itemLiveData;
    }

    public String getRetrofit_method() {
        return retrofit_method;
    }
}
