package com.example.administrator.datasource.restaurant;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.retrofit.response.Restaurant;

public class RestaurantDataSourceFactory extends DataSource.Factory {
    private MutableLiveData<PageKeyedDataSource<Integer, Restaurant>> restaurantLiveData = new MutableLiveData<>();
    private String retrofit_method = ClaseGlobal.GET_ALL_RESTAURANTS;
    private String texto_busqueda = "";

    public RestaurantDataSourceFactory() {
    }

    public RestaurantDataSourceFactory(String retrofit_method, String texto_busqueda) {
        this.retrofit_method = retrofit_method;
        this.texto_busqueda = texto_busqueda;
    }

    @Override
    public DataSource create() {
        RestaurantDataSource restaurantDataSource = new RestaurantDataSource(this.retrofit_method, this.texto_busqueda);
        restaurantLiveData.postValue(restaurantDataSource);

        return restaurantDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, Restaurant>> getRestaurantLiveData() {
        return restaurantLiveData;
    }

    public String getRetrofit_method() {
        return retrofit_method;
    }
}
