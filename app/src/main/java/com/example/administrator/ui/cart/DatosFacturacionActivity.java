package com.example.administrator.ui.cart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.config.Utility;
import com.example.administrator.data.PedidoViewModel;
import com.example.administrator.domain.Client;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.request.MyCustomRequest;
import com.example.administrator.retrofit.response.Item;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class DatosFacturacionActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etIdentificacion, etNombres, etApellidos, etEmail, etDireccion, etTelefono;
    private TextView tvSubtotal, tvIva, tvTotal;
    private  Button btnRealizarPedido;
    private RadioGroup rgOpcionesEntrega;
    private ImageView ivBackButton;

    private SharedPreference sharedPreferences;
    private String desglosaIVA;
    private Client cliente, clienteDatosFacturacion;
    private ClaseGlobal claseGlobal;
    private Context context;
    private HashMap<Integer, Item> mapItems;
    String jsonCoordenadaDestino, jsonItemsFactura, jsonClienteFacturacion, opcionEntrega;

    private double totalFactura;

    private ProgressDialog progressDialog;

    PedidoViewModel pedidoViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_facturacion);

        ivBackButton = findViewById(R.id.ivBackDatosFacturacion);
        etIdentificacion = findViewById(R.id.etIdentificacionDatosFactura);
        etNombres = findViewById(R.id.etNombresDatosFactura);
        etApellidos = findViewById(R.id.etApellidosDatosFactura);
        etEmail = findViewById(R.id.etEmailDatosFactura);
        etDireccion = findViewById(R.id.etDireccionDatosFactura);
        etTelefono = findViewById(R.id.etTelefonoDatosFactura);
        rgOpcionesEntrega = findViewById(R.id.rgOpcionesEntrega);

        tvSubtotal = findViewById(R.id.tvSubtotalDatosFacturacion);
        tvIva = findViewById(R.id.tvIVADatosFacturacion);
        tvTotal = findViewById(R.id.tvTotalDatosFacturacion);

        btnRealizarPedido = findViewById(R.id.btnRealizarPedido);

        claseGlobal = (ClaseGlobal) getApplicationContext();
        context = this;
        mapItems = claseGlobal.getMapItems();
        sharedPreferences = new SharedPreference();
        pedidoViewModel = ViewModelProviders.of(this).get(PedidoViewModel.class);

        desglosaIVA = sharedPreferences.getValue(context, "desglosaIva", "");

        String jsonClientPref = sharedPreferences.getValue(context, "client", "");
        Gson gson = new Gson();
        cliente = gson.fromJson(jsonClientPref, Client.class);

        jsonCoordenadaDestino = sharedPreferences.getValue(context, "coordenada_destino", "");
        jsonItemsFactura = sharedPreferences.getValue(context, "items", "");

        double subtotal = 0, IVA;

        for (Item item : mapItems.values()) {
            //CALCULO DESGLOSANDO IVA
            if (desglosaIVA.equals("S")) {
                subtotal = subtotal + ((Integer.parseInt(item.getQuantity())*item.getPrice()) - item.getPrice() * Integer.parseInt(item.getQuantity()) * (cliente.getDescuento()/100))/1.12;
            } else if(desglosaIVA.equals("N")) {
                //CALCULO SIN DESGLOSAR IVA
                subtotal = subtotal + ((Integer.parseInt(item.getQuantity())*item.getPrice()) - item.getPrice() * Integer.parseInt(item.getQuantity()) * (cliente.getDescuento()/100));
            }
        }

        DecimalFormat df = new DecimalFormat("#.00");
        IVA = subtotal * 0.12;
        tvSubtotal.setText(df.format(subtotal) + " $");
        tvIva.setText(df.format(IVA) + " $");
        tvTotal.setText(df.format(subtotal + IVA) + " $");

        totalFactura = subtotal + IVA;

        ivBackButton.setOnClickListener(this);
        btnRealizarPedido.setOnClickListener(this);

        etIdentificacion.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if (etIdentificacion.getText().toString().equals(cliente.getCodigo())) {
                        etNombres.setText(cliente.getNombre());
                        etApellidos.setText(cliente.getApellido());
                        etDireccion.setText(cliente.getDireccionDomicilio());
                        etEmail.setText(cliente.getCorreo());
                        etTelefono.setText(cliente.getTelefono());
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRealizarPedido: {
                boolean camposTextosLlenos = true;
                // Agrego validaciones
                if (etIdentificacion.getText().toString().isEmpty()) {
                    etIdentificacion.setError("Número de identificación es requerido.");
                    camposTextosLlenos = false;
                }

                if (!etIdentificacion.getText().toString().equals("9999999999") && !etIdentificacion.getText().toString().equals("9999999999999")) {
                    if (!Utility.VerificaIdentificacion(etIdentificacion.getText().toString())) {
                        etIdentificacion.setError("Número de identificación no es valido.");
                        camposTextosLlenos = false;
                    }
                }

                if (etNombres.getText().toString().isEmpty()) {
                    etNombres.setError("Nombre del cliente es requerido.");
                    camposTextosLlenos = false;
                }

                if (etApellidos.getText().toString().isEmpty()) {
                    etApellidos.setError("Apellido del cliente es requerido.");
                    camposTextosLlenos = false;
                }

                if (etEmail.getText().toString().isEmpty()) {
                    etEmail.setError("Correo electrónico es requerido.");
                    camposTextosLlenos = false;
                }

                if (etDireccion.getText().toString().isEmpty()) {
                    etDireccion.setError("Dirección de domicilio es requerido.");
                    camposTextosLlenos = false;
                }

                if (etTelefono.getText().toString().isEmpty()) {
                    etTelefono.setError("Teléfono es requerido.");
                    camposTextosLlenos = false;
                }

                if (!camposTextosLlenos) {
                    return;
                }

                if ((etIdentificacion.getText().toString().equals("9999999999") || etIdentificacion.getText().toString().equals("9999999999999"))
                        & totalFactura > 200) {
                    AlertDialog.Builder alertError = new AlertDialog.Builder(this);
                    alertError.setTitle("Elija dirección");
                    alertError.setMessage("No puede facturar con un monto mayor a 200$ a nombre de CONSUMIDOR FINAL, escriba un numero de cédula o ruc para montos mayores a 200$.");
                    alertError.setPositiveButton("OK",null);
                    alertError.show();
                    return;
                }

                final AlertDialog.Builder alertPedido = new AlertDialog.Builder(this);
                alertPedido.setTitle("Precaución");
                alertPedido.setMessage("¿Está seguro de finalizar el pedido?");
                alertPedido.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        progressDialog = new ProgressDialog(context);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.show();
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage("Realizando pedido...");

                        rgOpcionesEntrega.getCheckedRadioButtonId();
                        if (rgOpcionesEntrega.getCheckedRadioButtonId() == R.id.rbOpcionParaLlevar) {
                            opcionEntrega = ClaseGlobal.PEDIDO_LLEVAR;
                        } else {
                            opcionEntrega = ClaseGlobal.PEDIDO_DOMICILIO;
                        }

                        clienteDatosFacturacion = new Client(
                                etIdentificacion.getText().toString(),
                                "",
                                etNombres.getText().toString(),
                                etApellidos.getText().toString(),
                                "",
                                0.0,
                                etEmail.getText().toString(),
                                etTelefono.getText().toString(),
                                etDireccion.getText().toString());

                        Gson gson = new Gson();
                        jsonClienteFacturacion = gson.toJson(clienteDatosFacturacion);

                        String urlPedido = ClaseGlobal.getInstance().GET_METHOD_INSERT_PEDIDO();
                        String tag_json_obj = "json_obj_req";

                        MyCustomRequest postRequest = new MyCustomRequest(Request.Method.POST, urlPedido,
                                context, new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response.toString());
                                    String success = jsonObject.getString("data");

                                    if (success.equals("true")) {
                                        // Vacío el carrito de compras.
                                        pedidoViewModel.deleteAllDetallesPedido();

                                        AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                        alertError.setTitle("Mensaje");
                                        alertError.setMessage("Pedido realizado con éxito.");
                                        alertError.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                onBackPressed();
                                            }
                                        });
                                        alertError.show();
                                    } else {
                                        AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                        alertError.setTitle("Error en Cliente");
                                        alertError.setMessage("Error al realizar pedido. Contacte a soporte. ");
                                        alertError.setPositiveButton("OK",null);
                                        alertError.show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                    alertError.setTitle("Error de excepción");
                                    alertError.setMessage("Error al realizar pedido. Contacte a soporte. EXCEPCION. " + e.getMessage());
                                    alertError.setPositiveButton("OK",null);
                                    alertError.show();
                                }
                                progressDialog.dismiss();
                            }
                        },
                                new Response.ErrorListener()
                                {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        // error
                                        AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                                        alertErrorResponse.setTitle("Error en Servidor");
                                        alertErrorResponse.setMessage("Error al realizar pedido. Contacte a soporte. " + error.getMessage());
                                        alertErrorResponse.setPositiveButton("OK",null);
                                        alertErrorResponse.show();
                                        progressDialog.dismiss();
                                    }
                                }
                        ) {
                            @Override
                            protected Map<String, String> getParams()
                            {
                                Map<String, String>  params = new HashMap<>();

                                String codigo = "";
                                if (cliente.getCodigo().length() == 10) {
                                    codigo = cliente.getCodigo();
                                } else if(cliente.getRuc().length() == 13) {
                                    codigo = cliente.getRuc();
                                }

                                params.put("cliente_logueado", codigo);
                                params.put("cliente", jsonClienteFacturacion);
                                params.put("items", jsonItemsFactura);
                                params.put("coordenadas_destino", jsonCoordenadaDestino);
                                params.put("opcion_entrega", opcionEntrega);
                                return params;
                            }
                        };
                        postRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Adding request to request queue
                        ClaseGlobal.getInstance().addToRequestQueue(postRequest, tag_json_obj);
                    }
                });
                alertPedido.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });
                alertPedido.show();
                break;
            }
            case R.id.ivBackDatosFacturacion: {
                onBackPressed();
                break;
            }
        }
    }
}
