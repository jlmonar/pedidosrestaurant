package com.example.administrator.request;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.administrator.pedidosapp.R;
import com.google.gson.Gson;
import android.content.Context;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class MyCustomRequest extends Request {
    private static final String TAG = "MYCUSTOMREQUEST_TAG";
    private Response.Listener listener;

    private Context context;

    /**
     *
     * @param method Method- GET,POST
     * @param url URL
     * @param context CONTEXT
     * @param listener listener to notify success response
     * @param errorListener listener to notify error response
     */
    public MyCustomRequest(int method, String url, Context context, Response.Listener listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.listener = listener;
        this.context = context;
    }

    /**
     * This method needs to be implemented to parse the raw network response
     * and return an appropriate response type.This method will be called
     * from a worker thread. The response will not be delivered if you return null.
     * @param response  Response- Response payload as byte[],headers and status code
     * @return
     */
    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        try {
            /**
             First you will have to convert the NetworkResponse into a jsonstring.
             Then that json string can be converted into the required java object
             using gson
             **/
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));

            //return Response.success(gson.fromJson(jsonString, responseClass), HttpHeaderParser.parseCacheHeaders(response));
            return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }

    /**
     This is called on the main thread with the object you returned in
     parseNetworkResponse(). You should be invoking your callback interface
     from here
     **/
    @Override
    protected void deliverResponse(Object response) {
        listener.onResponse(response);
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        return new VolleyError(getMessage(volleyError, context));
    }

    public  String getMessage (Object error , Context context){
        if(error instanceof TimeoutError){
            return context.getResources().getString(R.string.timeout);
        } else if(isNetworkProblem(error)){
            return context.getResources().getString(R.string.nointernet);
        } else if (error instanceof ParseError) {
            return context.getResources().getString(R.string.parse_error);
        } else if (isServerProblem(error)){
            return handleServerError(error ,context);
        }
        return context.getResources().getString(R.string.generic_error);
    }

    private String handleServerError(Object error, Context context) {
        VolleyError volleyError = (VolleyError)error;
        NetworkResponse response = volleyError.networkResponse;
        if(response != null && response.data != null){
            try {
                // Errores a mostrar para modo de DESARROLLO.
                String responseErrorJson = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                JSONObject jsonObject = new JSONObject(responseErrorJson);
                String errorMessage = jsonObject.getString("error");

                return errorMessage;

                // Errores a mostrar para modo de PRODUCCIÓN.
                /*
                switch (response.statusCode){
                    case 401: {
                        return "No está autenticado";
                    }
                    case 403: {
                        return "No posee permision para ejecutar esta acción";
                    }
                    case 404:
                    {
                        return "No se ha encontrado instancia con el id proporcionado.";
                    }
                    case 405: {
                        return "El método especificado en la petición es invalido.";
                    }
                    case 500: {
                        return "Error inesperado, intente más tarde";
                    }

                    default:
                        return context.getResources().getString(R.string.timeout);
                }*/
            } catch (Exception e) {
                return e.getMessage();
            }
        }

        return context.getResources().getString(R.string.generic_error);
    }

    private boolean isServerProblem(Object error) {
        return (error instanceof ServerError || error instanceof AuthFailureError);
    }

    private boolean isNetworkProblem (Object error){
        return (error instanceof NetworkError || error instanceof NoConnectionError);
    }
}
