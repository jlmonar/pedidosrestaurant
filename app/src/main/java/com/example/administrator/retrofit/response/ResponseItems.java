
package com.example.administrator.retrofit.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseItems {

    @SerializedName("data")
    @Expose
    private List<Item> data = null;
    @SerializedName("meta")
    @Expose
    private Meta meta;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ResponseItems() {
    }

    /**
     * 
     * @param data
     * @param meta
     */
    public ResponseItems(List<Item> data, Meta meta) {
        super();
        this.data = data;
        this.meta = meta;
    }

    public List<Item> getData() {
        return data;
    }

    public void setData(List<Item> data) {
        this.data = data;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

}
