package com.example.administrator.domain;

/**
 * Created by Administrator on 14/03/2018.
 */

public class Client {
    private String codigo;
    private String ruc;
    private String nombre;
    private String apellido;
    private String tipo;
    private Double descuento;
    private String correo;
    private String telefono;
    private String direccionDomicilio;

    public Client(String codigo, String ruc, String nombre, String apellido, String tipo, Double descuento, String correo, String telefono) {
        this.codigo = codigo;
        this.ruc = ruc;
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipo = tipo;
        this.descuento = descuento;
        this.correo = correo;
        this.telefono = telefono;
    }

    public Client(String codigo, String ruc, String nombre, String apellido, String tipo, Double descuento, String correo, String telefono, String direccionDomicilio) {
        this.codigo = codigo;
        this.ruc = ruc;
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipo = tipo;
        this.descuento = descuento;
        this.correo = correo;
        this.telefono = telefono;
        this.direccionDomicilio = direccionDomicilio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getDescuento() {
        return descuento;
    }

    public void setDescuento(Double descuento) {
        this.descuento = descuento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccionDomicilio() {
        return direccionDomicilio;
    }

    public void setDireccionDomicilio(String direccionDomicilio) {
        this.direccionDomicilio = direccionDomicilio;
    }
}
