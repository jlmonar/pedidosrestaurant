package com.example.administrator.ui.cart;

import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.data.PedidoViewModel;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.retrofit.response.Item;

import java.text.DecimalFormat;
import java.util.List;

public class DetallePedidoRecyclerViewAdapter extends RecyclerView.Adapter<DetallePedidoRecyclerViewAdapter.ViewHolder> {
    Context context;
    private List<Item> mValues;
    private final String desglosaIVA;
    private final Double descuentoCliente;
    PedidoViewModel pedidoViewModel;

    public DetallePedidoRecyclerViewAdapter(Context context, List<Item> items,  String desglosaIVA, Double descuentoCliente) {
        this.context = context;
        mValues = items;
        this.desglosaIVA = desglosaIVA;
        this.descuentoCliente = descuentoCliente;
        pedidoViewModel = ViewModelProviders.of((FragmentActivity) context).get(PedidoViewModel.class);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_detalle_pedido, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.tvCantidad.setText(holder.mItem.getQuantity());
        holder.tvDescripcion.setText(holder.mItem.getTitle());

        DecimalFormat df = new DecimalFormat("#.00");
        double itemPrice = desglosaIVA.equals("S") ? holder.mItem.getPrice()/1.12 : holder.mItem.getPrice();
        double descuento = desglosaIVA.equals("S") ?
                (holder.mItem.getPrice() * (descuentoCliente/100) * Integer.parseInt(holder.mItem.getQuantity()))/1.12 :
                holder.mItem.getPrice() * (descuentoCliente/100) * Integer.parseInt(holder.mItem.getQuantity());

        double total = (itemPrice * Integer.parseInt(holder.mItem.getQuantity())) - descuento;

        holder.tvPrecio.setText(df.format(total) + " $" );

        holder.ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertPedido = new AlertDialog.Builder(context);
                alertPedido.setTitle("Precaución");
                alertPedido.setMessage("Va a eliminar el item " + holder.mItem.getTitle() + " del carrito de pedidos. ¿Desea continuar?");
                alertPedido.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        pedidoViewModel.deleteDetallePedido(holder.mItem.getIdentifier());
                    }
                });
                alertPedido.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });
                alertPedido.show();
            }
        });


        holder.ivMinusDetallePedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int cantidad = Integer.parseInt(holder.mItem.getQuantity()) - 1;
                if (cantidad > 0) {
                    pedidoViewModel.updateDetallePedido(holder.mItem.getIdentifier(), ""+cantidad);
                }
            }
        });

        holder.ivPlusDetallePedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int cantidad = Integer.parseInt(holder.mItem.getQuantity()) + 1;
                pedidoViewModel.updateDetallePedido(holder.mItem.getIdentifier(), ""+cantidad);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvCantidad, tvDescripcion, tvPrecio;
        public final ImageButton ibDelete;
        public final ImageView ivPlusDetallePedido, ivMinusDetallePedido;
        public Item mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvCantidad = view.findViewById(R.id.tvCantidad);
            tvDescripcion = view.findViewById(R.id.tvDescripcion);
            tvPrecio = view.findViewById(R.id.tvPrecio);
            ibDelete = view.findViewById(R.id.ivDelete);
            ivPlusDetallePedido = view.findViewById(R.id.ivPlusDetallePedido);
            ivMinusDetallePedido = view.findViewById(R.id.ivMinusDetallePedido);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvDescripcion.getText() + "'";
        }
    }

    public void setData(List<Item> detallesPedido) {
        mValues = detallesPedido;
        notifyDataSetChanged();
    }
}
