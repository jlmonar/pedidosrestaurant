package com.example.administrator.ui.order;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.domain.Client;
import com.example.administrator.domain.Pedido;
import com.example.administrator.pedidosapp.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HistorialPedidosFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HistorialPedidosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistorialPedidosFragment extends Fragment implements View.OnClickListener{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private EditText etInitialDate, etEndDate;
    private Button btnConsultarPedidos;

    private SharedPreference sharedPreferences;

    private ProgressDialog progressDialog;
    private ProgressDialog progressDialogConsultaDetallePedido;
    private TableLayout pedidosTable;

    private static final String CERO = "0";
    private static final String BARRA = "/";

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la fecha
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);


    private OnFragmentInteractionListener mListener;

    public HistorialPedidosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HistorialPedidosFragment.
     */
    public static HistorialPedidosFragment newInstance(String param1, String param2) {
        HistorialPedidosFragment fragment = new HistorialPedidosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_historial_pedidos, container, false);

        sharedPreferences = new SharedPreference();

        etInitialDate = (EditText) view.findViewById(R.id.etInitialDate);
        etEndDate = (EditText) view.findViewById(R.id.etEndDate);

        pedidosTable = (TableLayout) view.findViewById(R.id.pedidosTable);

        btnConsultarPedidos = (Button) view.findViewById(R.id.btnConsultarPedidos);

        etInitialDate.setOnClickListener(this);
        etEndDate.setOnClickListener(this);
        btnConsultarPedidos.setOnClickListener(this);
        // Inflate the layout for this fragment
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etInitialDate: {
                obtenerFecha(etInitialDate);
                break;
            }
            case R.id.etEndDate: {
                obtenerFecha(etEndDate);
                break;
            }
            case R.id.btnConsultarPedidos: {
                if (etInitialDate.getText().toString().equals("") || etEndDate.getText().toString().equals("")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Precaución.");
                    alert.setMessage("Selecciona fecha de inicio y fecha fin antes de continuar.");
                    alert.setPositiveButton("OK",null);
                    alert.show();
                } else {
                    //Remover items de busqueda previa
                    pedidosTable.removeAllViews();

                    String jsonClientPref = sharedPreferences.getValue(v.getContext(), "client", "");

                    Gson gson = new Gson();
                    Client clientFromJSON = gson.fromJson(jsonClientPref, Client.class);

                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Consultando Pedidos...");

                    String cliente = "";
                    if (clientFromJSON.getCodigo().length() == 10) {
                        cliente = clientFromJSON.getCodigo();
                    } else if (clientFromJSON.getRuc().length() == 13) {
                        cliente = clientFromJSON.getRuc();
                    }

                    String url = ClaseGlobal.getInstance().GET_METHOD_GET_PEDIDOS_BY_DATE()
                            + (etInitialDate.getText().toString()).replace("/", "") + "/"
                            + (etEndDate.getText().toString()).replace("/", "") + "/"
                            + cliente + "?per_page=0";

                    // Tag used to cancel the request
                    String  tag_string_req = "string_req";

                    // TODO Usar Request Customizado (MyCustomRequest) para todas las peticiones.
                    StringRequest strReq = new StringRequest(Request.Method.GET,
                            url, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.toString());
                                int color = 0;
                                JSONArray dataJSONArray = jsonObject.getJSONArray("data");
                                if (dataJSONArray.length() > 0) {
                                    for ( int i=0; i < dataJSONArray.length(); i++) {
                                        JSONObject pedidoJSONObject = dataJSONArray.getJSONObject(i);

                                        Pedido pedido = new Pedido(
                                                pedidoJSONObject.getString("id"),
                                                pedidoJSONObject.getString("fecha"),
                                                pedidoJSONObject.getDouble("total"),
                                                pedidoJSONObject.getString("vendedor"),
                                                pedidoJSONObject.getString("contacto_vendedor"));

                                        color = ((i+1)%2 == 0) ? Color.WHITE: Color.parseColor("#daeaec");

                                        addTableRow(pedidosTable, pedido, color);
                                    }
                                } else {
                                    AlertDialog.Builder alertError = new AlertDialog.Builder(getActivity());
                                    alertError.setTitle("Sin resultados");
                                    alertError.setMessage("No se encontraron resultados.");
                                    alertError.setPositiveButton("OK",null);
                                    alertError.show();
                                }
                            } catch (JSONException e) {
                                AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(getActivity());
                                alertExcepcion.setTitle("Error");
                                alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                                alertExcepcion.setPositiveButton("OK",null);
                                alertExcepcion.show();

                                e.printStackTrace();
                            }
                            progressDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(getActivity());
                            alertErrorResponse.setTitle("Error");
                            alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                            alertErrorResponse.setPositiveButton("OK",null);
                            alertErrorResponse.show();
                            progressDialog.dismiss();
                        }
                    });
                    // Adding request to request queue
                    ClaseGlobal.getInstance().addToRequestQueue(strReq, tag_string_req);
                }

                break;
            }
        }
    }

    private void obtenerFecha(final EditText etDate){
        DatePickerDialog recogerFecha = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                //Formateo el día obtenido: antepone el 0 si son menores de 10
                String diaFormateado = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10)? CERO + String.valueOf(mesActual):String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                etDate.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);
            }
            //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
            /**
             *También puede cargar los valores que usted desee
             */
        },anio, mes, dia);
        //Muestro el widget
        recogerFecha.show();
    }

    public void addTableRow(TableLayout tableLayout, final Pedido pedido, int color) {
        int paddingTop = 40;
        int textSize = 15;

        TableRow tableRow = new TableRow(getActivity().getApplicationContext());
        tableRow.setBackgroundColor(color);
        tableRow.setClickable(true);

        TableRow.LayoutParams trLayoutParams1;
        trLayoutParams1 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams1.weight = 1; //column weight

        TextView tv1 = new TextView(getActivity().getApplicationContext());
        tv1.setPadding(0,paddingTop,0,paddingTop);
        tv1.setTextColor(Color.BLACK);
        tv1.setTextSize(textSize);
        tv1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv1.setGravity(Gravity.CENTER);
        tv1.setText(pedido.getSecuencia());
        tv1.setLayoutParams(trLayoutParams1);

        TableRow.LayoutParams trLayoutParams2;
        trLayoutParams2 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams2.weight = (float) 1.8;

        TextView tv2 = new TextView(getActivity().getApplicationContext());
        tv2.setPadding(0,paddingTop,0,paddingTop);
        tv2.setTextColor(Color.BLACK);
        tv2.setTextSize(textSize);
        tv2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv2.setGravity(Gravity.CENTER);
        tv2.setText(pedido.getFecha());
        tv2.setLayoutParams(trLayoutParams2);

        TableRow.LayoutParams trLayoutParams3;
        trLayoutParams3 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams3.weight = (float) 1.2;

        TextView tv3 = new TextView(getActivity().getApplicationContext());
        tv3.setPadding(0,paddingTop,0,paddingTop);
        tv3.setTextColor(Color.BLACK);
        tv3.setTextSize(textSize);
        tv3.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv3.setGravity(Gravity.CENTER);
        DecimalFormat df = new DecimalFormat("#.00");
        tv3.setText("" + df.format(pedido.getValorTotal() + pedido.getValorTotal()*0.12));
        tv3.setLayoutParams(trLayoutParams3);

        //ClickListener al tableRow
        tableRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialogConsultaDetallePedido = new ProgressDialog(getActivity());
                progressDialogConsultaDetallePedido.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialogConsultaDetallePedido.show();
                progressDialogConsultaDetallePedido.setCancelable(false);
                progressDialogConsultaDetallePedido.setMessage("Consultando pedido...");

                String url = ClaseGlobal.getInstance().GET_METHOD_DETALLES_PEDIDO()
                        + pedido.getSecuencia() + "?per_page=0";

                // Tag used to cancel the request
                String  tag_string_req = "string_req";

                // TODO Usar Request Customizado (MyCustomRequest) para todas las peticiones.
                StringRequest strReq = new StringRequest(Request.Method.GET,
                        url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONArray dataJSONArray = jsonObject.getJSONArray("data");

                            if (dataJSONArray.length() > 0) {
                                Intent detailPedidoActivity = new Intent(getActivity().getApplicationContext(), DetallePedidoActivity.class);
                                detailPedidoActivity.putExtra("detalles_pedido", response.toString());
                                detailPedidoActivity.putExtra("id_pedido", pedido.getSecuencia());
                                detailPedidoActivity.putExtra("fecha", pedido.getFecha());
                                detailPedidoActivity.putExtra("valor_total", "" + pedido.getValorTotal());
                                detailPedidoActivity.putExtra("nombreVendedor", pedido.getNombreVendedor());
                                detailPedidoActivity.putExtra("telefonoVendedor", pedido.getTelefonoVendedor());

                                startActivity(detailPedidoActivity);
                            } else {
                                AlertDialog.Builder alertError = new AlertDialog.Builder(getActivity());
                                alertError.setTitle("Sin resultados");
                                alertError.setMessage("No se encontraron resultados.");
                                alertError.setPositiveButton("OK",null);
                                alertError.show();
                            }
                        } catch (JSONException e) {
                            AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(getActivity());
                            alertExcepcion.setTitle("Error");
                            alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                            alertExcepcion.setPositiveButton("OK",null);
                            alertExcepcion.show();

                            e.printStackTrace();
                        } finally {
                            progressDialogConsultaDetallePedido.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(getActivity());
                        alertErrorResponse.setTitle("Error");
                        alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                        alertErrorResponse.setPositiveButton("OK",null);
                        alertErrorResponse.show();
                        progressDialogConsultaDetallePedido.dismiss();
                    }
                });
                // Adding request to request queue
                ClaseGlobal.getInstance().addToRequestQueue(strReq, tag_string_req);
            }
        });

        tableRow.addView(tv1);
        tableRow.addView(tv2);
        tableRow.addView(tv3);

        tableLayout.addView(tableRow);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
