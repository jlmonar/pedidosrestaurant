package com.example.administrator.ui.cart;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.data.PedidoViewModel;
import com.example.administrator.domain.Client;
import com.example.administrator.domain.Coordenada;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.retrofit.response.Item;
import com.example.administrator.ui.map.DestinoEntregaMapActivity;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class CarritoPedidoActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout layoutBottomShoppingCart;
    private ClaseGlobal claseGlobal;
    private Context context;
    private SharedPreference sharedPreferences;

    private TextView tvSubtotal, tvIVA, tvTotalCost, tvMessageEmptyCart, tvDireccionEntrega;
    private Button btnCambiarDireccionDestino;

    private Button btnContinuarPedido;

    private ImageView ivBack;

    private Client cliente;
    private String desglosaIVA;

    PedidoViewModel pedidoViewModel;
    private DetallePedidoRecyclerViewAdapter adapter;
    private RecyclerView recyclerviewDetallesPedido;
    private HashMap<Integer, Item> mapItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito_pedido);

        context = this;

        pedidoViewModel = ViewModelProviders.of(this).get(PedidoViewModel.class);

        layoutBottomShoppingCart = findViewById(R.id.layoutBottomShoppingCart);
        recyclerviewDetallesPedido = findViewById(R.id.recyclerViewDetallesPedido);

        claseGlobal = (ClaseGlobal) getApplicationContext();
        mapItems = claseGlobal.getMapItems();
        sharedPreferences = new SharedPreference();

        desglosaIVA = sharedPreferences.getValue(context, "desglosaIva", "");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        tvSubtotal = findViewById(R.id.tvSubtotal);
        tvIVA = findViewById(R.id.tvIVA);
        tvTotalCost = findViewById(R.id.tvTotal);
        tvMessageEmptyCart = findViewById(R.id.tvMessageEmptyCart);
        tvDireccionEntrega = findViewById(R.id.tvDireccionEntrega);

        btnContinuarPedido = findViewById(R.id.btnContinuarPedido);
        btnContinuarPedido.setOnClickListener(this);

        btnCambiarDireccionDestino = findViewById(R.id.btnCambiarDireccionEntrega);
        btnCambiarDireccionDestino.setOnClickListener(this);

        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(this);

        if (mapItems.size() == 0) {
            tvMessageEmptyCart.setVisibility(View.VISIBLE);
            recyclerviewDetallesPedido.setVisibility(View.GONE);
            layoutBottomShoppingCart.setVisibility(View.GONE);
        } else {
            tvMessageEmptyCart.setVisibility(View.GONE);
            recyclerviewDetallesPedido.setVisibility(View.VISIBLE);
            layoutBottomShoppingCart.setVisibility(View.VISIBLE);
        }

        String jsonClientPref = sharedPreferences.getValue(context, "client", "");
        Gson gson = new Gson();
        cliente = gson.fromJson(jsonClientPref, Client.class);

        // Obtengo la dirección de entrega guardada localmente, si es que ha sido seleccionada con anterioridad.
        String jsonCoordenadaDestino = sharedPreferences.getValue(context, "coordenada_destino", "");
        if (!jsonCoordenadaDestino.equals("")) {
            Coordenada coordenada_destino = gson.fromJson(jsonCoordenadaDestino, Coordenada.class);
            tvDireccionEntrega.setText(getStringAddress(coordenada_destino.getLatitud(), coordenada_destino.getLongitud()));
        }

        double subtotal = 0, IVA = 0;
        List<Item> sortedItemsList = sortHashMapItems(mapItems);
        for (Item item : sortedItemsList) {
            //CALCULO DESGLOSANDO IVA
            if (desglosaIVA.equals("S")) {
                subtotal = subtotal + ((Integer.parseInt(item.getQuantity())*item.getPrice()) - item.getPrice() * Integer.parseInt(item.getQuantity()) * (cliente.getDescuento()/100))/1.12;
            } else if(desglosaIVA.equals("N")) {
                //CALCULO SIN DESGLOSAR IVA
                subtotal = subtotal + ((Integer.parseInt(item.getQuantity())*item.getPrice()) - item.getPrice() * Integer.parseInt(item.getQuantity()) * (cliente.getDescuento()/100));
            }
        }

        DecimalFormat df = new DecimalFormat("#.00");
        IVA = subtotal * 0.12;
        tvSubtotal.setText(df.format(subtotal) + " $");
        tvIVA.setText(df.format(IVA) + " $");
        tvTotalCost.setText(df.format(subtotal + IVA) + " $");

        // SETEO EL ADAPTER
        recyclerviewDetallesPedido.setLayoutManager(new LinearLayoutManager(context));;
        adapter = new DetallePedidoRecyclerViewAdapter(this, sortedItemsList, desglosaIVA, cliente.getDescuento());
        recyclerviewDetallesPedido.setAdapter(adapter);

        pedidoViewModel.getDetallesPedido().observe(this, new Observer<HashMap<Integer, Item>>() {
            @Override
            public void onChanged(@Nullable HashMap<Integer, Item> detallesPedidoHashMap) {
                mapItems = detallesPedidoHashMap;

                if (mapItems.size() == 0) {
                    tvMessageEmptyCart.setVisibility(View.VISIBLE);
                    recyclerviewDetallesPedido.setVisibility(View.GONE);
                    layoutBottomShoppingCart.setVisibility(View.GONE);
                } else {
                    tvMessageEmptyCart.setVisibility(View.GONE);
                    recyclerviewDetallesPedido.setVisibility(View.VISIBLE);
                    layoutBottomShoppingCart.setVisibility(View.VISIBLE);
                }

                List<Item> sortedDetallesPedidoList = sortHashMapItems(mapItems);
                adapter.setData(sortedDetallesPedidoList);

                double subtotal = 0, IVA = 0;
                for (Item item : sortedDetallesPedidoList) {
                    //CALCULO DESGLOSANDO IVA
                    if (desglosaIVA.equals("S")) {
                        subtotal = subtotal + ((Integer.parseInt(item.getQuantity())*item.getPrice()) - item.getPrice() * Integer.parseInt(item.getQuantity()) * (cliente.getDescuento()/100))/1.12;
                    } else if(desglosaIVA.equals("N")) {
                        //CALCULO SIN DESGLOSAR IVA
                        subtotal = subtotal + ((Integer.parseInt(item.getQuantity())*item.getPrice()) - item.getPrice() * Integer.parseInt(item.getQuantity()) * (cliente.getDescuento()/100));
                    }
                }

                DecimalFormat df = new DecimalFormat("#.00");
                IVA = subtotal * 0.12;
                tvSubtotal.setText(df.format(subtotal) + " $");
                tvIVA.setText(df.format(IVA)  + " $");
                tvTotalCost.setText(df.format(subtotal + IVA)  + " $");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnContinuarPedido: {
                String jsonCoordenadaDestino = sharedPreferences.getValue(context, "coordenada_destino", "");
                if (jsonCoordenadaDestino.isEmpty()) {
                    AlertDialog.Builder alertError = new AlertDialog.Builder(this);
                    alertError.setTitle("Elija dirección");
                    alertError.setMessage("Por favor, elija del mapa una dirección de entrega.");
                    alertError.setPositiveButton("OK",null);
                    alertError.show();
                    return;
                }
                Intent datosFacturacionActivity = new Intent(context, DatosFacturacionActivity.class);
                startActivity(datosFacturacionActivity);

                break;
            }
            case R.id.btnCambiarDireccionEntrega: {
                Intent cambiarDestinoEntregaIntent = new Intent(context, DestinoEntregaMapActivity.class);
                String jsonCoordenadaDestino = sharedPreferences.getValue(context, "coordenada_destino", "");
                if (!jsonCoordenadaDestino.equals("")) {
                    cambiarDestinoEntregaIntent.putExtra("coordenada_destino", jsonCoordenadaDestino);
                }
                startActivity(cambiarDestinoEntregaIntent);
                break;
            }
            case R.id.ivBack: {
                onBackPressed();
                break;
            }
        }
    }

    public String getStringAddress(double latitude, double longitude) {
        String address = "";
        String city = "";
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);

            address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getLocality();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return address + " " + city;
    }

    public List<Item>  sortHashMapItems(HashMap<Integer, Item> unsortedHashMap) {
        List<Item> list = new ArrayList<>(unsortedHashMap.values());

        //How to sort a List<Object> alphabetically using Object name field
        //https://stackoverflow.com/a/8432848
        if (list.size() > 0) {
            Collections.sort(list, new Comparator<Item>() {
                @Override
                public int compare(final Item object1, final Item object2) {
                    return object1.getTitle().compareTo(object2.getTitle());
                }
            });
        }

        return list;
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();

        Gson gson = new Gson();
        String jsonCoordenadaDestino = sharedPreferences.getValue(context, "coordenada_destino", "");
        if (!jsonCoordenadaDestino.equals("")) {
            Coordenada coordenada_destino = gson.fromJson(jsonCoordenadaDestino, Coordenada.class);
            tvDireccionEntrega.setText(getStringAddress(coordenada_destino.getLatitud(), coordenada_destino.getLongitud()));
        }

        mapItems = claseGlobal.getMapItems();

        if (mapItems.size() == 0) {
            tvMessageEmptyCart.setVisibility(View.VISIBLE);
            recyclerviewDetallesPedido.setVisibility(View.GONE);
            layoutBottomShoppingCart.setVisibility(View.GONE);
        } else {
            tvMessageEmptyCart.setVisibility(View.GONE);
            recyclerviewDetallesPedido.setVisibility(View.VISIBLE);
            layoutBottomShoppingCart.setVisibility(View.VISIBLE);
        }
    }
}
