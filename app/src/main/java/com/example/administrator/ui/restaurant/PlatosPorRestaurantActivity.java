package com.example.administrator.ui.restaurant;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.Utility;
import com.example.administrator.domain.SubCategoria;
import com.example.administrator.layout.CustomConstraintLayout;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.request.MyCustomRequest;
import com.example.administrator.retrofit.response.Item;
import com.example.administrator.retrofit.response.Restaurant;
import com.example.administrator.ui.cart.CarritoPedidoActivity;
import com.example.administrator.ui.category.MySubCategoriaRecyclerViewAdapter;
import com.example.administrator.ui.food.MyPlatoComidaRecyclerViewAdapter;
import com.example.administrator.viewmodel.ItemViewModel;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.nikartm.support.ImageBadgeView;

public class PlatosPorRestaurantActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerViewPlatosComida, recyclerViewSubCategorias;
    private MyPlatoComidaRecyclerViewAdapter adaptadorPlatoComidaRestaurant;
    private MySubCategoriaRecyclerViewAdapter adaptadorSubcategorias;
    private List<SubCategoria> listSubCategorias;
    private Restaurant restaurant;
    private CustomConstraintLayout customLayout;
    private Context context;
    private ClaseGlobal claseGlobal;

    ItemViewModel itemViewModel;

    ProgressDialog progressDialog;

    private TextView tvRestaurant;
    private ImageView ivBackbutton;
    private ImageBadgeView ibvShoppingCartPlatoRestaurant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_platos_por_restaurant);

        claseGlobal = (ClaseGlobal)getApplicationContext();

        String jsonRestaurant = getIntent().getStringExtra("restaurant");
        Gson gson = new Gson();
        restaurant = gson.fromJson(jsonRestaurant, Restaurant.class);

        ivBackbutton = findViewById(R.id.ivBackButton);
        ivBackbutton.setOnClickListener(this);

        ibvShoppingCartPlatoRestaurant = findViewById(R.id.ibvShoppingCartPlatoRestaurante);
        ibvShoppingCartPlatoRestaurant.setOnClickListener(this);

        // Seteo el total de items en el carrito de compras.
        int totalItems = Utility.getTotalItemsCart(claseGlobal);
        if (totalItems == 0) {
            ibvShoppingCartPlatoRestaurant.clearBadge();
        } else {
            ibvShoppingCartPlatoRestaurant.setBadgeValue(totalItems);
        }

        tvRestaurant = findViewById(R.id.tvRestaurant);
        tvRestaurant.setText(restaurant.getName());

        customLayout = findViewById(R.id.layoutBackgroundRestaurantImage);

        String url_restaurant = ClaseGlobal.getInstance().GET_RESTAURANTES_URL() + "/" + restaurant.getCode() + "/restaurant.jpg";
        Picasso.get()
                .load(url_restaurant)
                .placeholder(R.drawable.default_restaurant)
                .error(R.drawable.default_restaurant)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                .resize(500, 375)
                .centerCrop()
                .into(customLayout);

        recyclerViewPlatosComida = findViewById(R.id.recyclerViewPlatosComidaRestaurante);
        recyclerViewSubCategorias = findViewById(R.id.recyclerViewSubCategorias);
        context = this;

        // Aqui cargo todos los items según el restaurant (linea) seleccionado.
        itemViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);

        adaptadorPlatoComidaRestaurant = new MyPlatoComidaRecyclerViewAdapter(this, ibvShoppingCartPlatoRestaurant);

        // Calculo el número de columnas que debe tener el grid según el ancho de la pantalla del teléfono.
        int nuberOfColumns = Utility.calculateNoOfColumns(this, 150);

        recyclerViewPlatosComida.setLayoutManager(new GridLayoutManager(this, nuberOfColumns));

        itemViewModel.setItemPagedList(ClaseGlobal.GET_ITEMS_BY_LINEA, restaurant.getCode(), "");
        itemViewModel.itemPagedList.observe(this, new Observer<PagedList<Item>>() {
            @Override
            public void onChanged(PagedList<Item> items) {
                adaptadorPlatoComidaRestaurant.submitList(items);
            }
        });

        recyclerViewPlatosComida.setAdapter(adaptadorPlatoComidaRestaurant);
        /////////////////////

        //Inicializo lista de categorias.
        listSubCategorias = new ArrayList<>();
        listSubCategorias.add(new SubCategoria("TODOS", "Todos"));

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Obteniendo información...");

        // Tag used to cancel the request
        final String  tag_string_req = "string_req";

        String urlGetSubcategorias =  ClaseGlobal.getInstance().GET_METHOD_SUBCATEGORIAS() + restaurant.getCode();

        MyCustomRequest strReqSubcategorias = new MyCustomRequest(Request.Method.GET,
                urlGetSubcategorias, context, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray dataJSONArray = jsonObject.getJSONArray("data");
                    if (dataJSONArray.length() > 0) {
                        for ( int i=0; i < dataJSONArray.length(); i++) {
                            JSONObject itemJSONObject = dataJSONArray.getJSONObject(i);

                            SubCategoria subcategoria = new SubCategoria(
                                    itemJSONObject.getString("code"),
                                    itemJSONObject.getString("name"));

                            listSubCategorias.add(subcategoria);
                        }
                        mostrarSubCategoriasPorLinea(listSubCategorias);


                    } else {
                        AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                        alertError.setTitle("Sin resultados");
                        alertError.setMessage("No se encontraron resultados.");
                        alertError.setPositiveButton("OK",null);
                        alertError.show();
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(context);
                    alertExcepcion.setTitle("Error");
                    alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                    alertExcepcion.setPositiveButton("OK",null);
                    alertExcepcion.show();

                    e.printStackTrace();
                }

                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                alertErrorResponse.setTitle("Error");
                alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                alertErrorResponse.setPositiveButton("OK",null);
                alertErrorResponse.show();
                progressDialog.dismiss();
            }
        });
        // Adding request to request queue
        ClaseGlobal.getInstance().addToRequestQueue(strReqSubcategorias, tag_string_req);
    }

    public void mostrarSubCategoriasPorLinea(List<SubCategoria> listaSubcategorias) {
        recyclerViewSubCategorias.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        adaptadorSubcategorias = new MySubCategoriaRecyclerViewAdapter(this, listaSubcategorias, recyclerViewPlatosComida, adaptadorPlatoComidaRestaurant, restaurant, itemViewModel);
        recyclerViewSubCategorias.setAdapter(adaptadorSubcategorias);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackButton: {
                onBackPressed();
                break;
            }
            case R.id.ibvShoppingCartPlatoRestaurante: {
                Intent activityShoppingCart = new Intent(this, CarritoPedidoActivity.class);
                startActivity(activityShoppingCart);
                break;
            }
        }
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();

        int totalItems = Utility.getTotalItemsCart(claseGlobal);
        if (totalItems == 0) {
            ibvShoppingCartPlatoRestaurant.clearBadge();
        } else {
            ibvShoppingCartPlatoRestaurant.setBadgeValue(totalItems);
        }

        adaptadorPlatoComidaRestaurant.notifyDataSetChanged();
        recyclerViewPlatosComida.setAdapter(adaptadorPlatoComidaRestaurant);
    }
}
