package com.example.administrator.ui.order;

import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.Utility;
import com.example.administrator.domain.DetallePedido;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.ui.cart.CarritoPedidoActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

import ru.nikartm.support.ImageBadgeView;

public class DetallePedidoActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvNumeroPedido, tvRepartidor, tvFecha, tvSubtotal, tvIVA, tvTotalVenta;
    private TableLayout detallesPedidoTable;
    private ImageView ivBackButton;
    private ImageBadgeView ibvShoppingCartDetallePedido;
    private ClaseGlobal claseGlobal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pedido);

        claseGlobal = (ClaseGlobal)getApplicationContext();

        String jsonDetallesPedido = getIntent().getStringExtra("detalles_pedido");
        String id_pedido = getIntent().getStringExtra("id_pedido");
        String fecha = getIntent().getStringExtra("fecha");
        String valor_total = getIntent().getStringExtra("valor_total");
        String nombreVendedor = getIntent().getStringExtra("nombreVendedor");
        String telefonoVendedor = getIntent().getStringExtra("telefonoVendedor");

        tvNumeroPedido = findViewById(R.id.tvNumeroPedido);
        tvRepartidor = findViewById(R.id.tvRepartidor);
        tvFecha = findViewById(R.id.tvFechaPedido);
        tvSubtotal = findViewById(R.id.tvSubtotalPedido);
        tvIVA =findViewById(R.id.tvIVAPedido);
        tvTotalVenta = findViewById(R.id.tvTotalVentaPedido);
        ivBackButton = findViewById(R.id.ivBackButton);
        ibvShoppingCartDetallePedido = findViewById(R.id.ibvShoppingCartDetallePedido);

        ivBackButton.setOnClickListener(this);
        ibvShoppingCartDetallePedido.setOnClickListener(this);

        // Seteo el total de items en el carrito de compras.
        int totalItems = Utility.getTotalItemsCart(claseGlobal);
        if (totalItems == 0) {
            ibvShoppingCartDetallePedido.clearBadge();
        } else {
            ibvShoppingCartDetallePedido.setBadgeValue(totalItems);
        }

        DecimalFormat df = new DecimalFormat("#.00");
        double subtotal =  Double.parseDouble(valor_total);
        double iva = subtotal * 0.12;
        double total = subtotal + iva;
        tvNumeroPedido.setText(tvNumeroPedido.getText() + id_pedido);
        tvRepartidor.setText(nombreVendedor + " - " + telefonoVendedor);
        tvFecha.setText(fecha);
        tvSubtotal.setText("" + df.format(subtotal));
        tvIVA.setText("" + df.format(iva));
        tvTotalVenta.setText("" + df.format(total));

        detallesPedidoTable = findViewById(R.id.detallesPedidoTable);

        try {
            JSONObject jsonObject = new JSONObject(jsonDetallesPedido);

            int color = 0;
            JSONArray dataJSONArray = jsonObject.getJSONArray("data");
            if (dataJSONArray.length() > 0) {
                for ( int i=0; i < dataJSONArray.length(); i++) {
                    JSONObject itemJSONObject = dataJSONArray.getJSONObject(i);

                    DetallePedido detallePedido = new DetallePedido(itemJSONObject.getString("linea"), itemJSONObject.getString("producto"), itemJSONObject.getInt("cantidad"),
                            itemJSONObject.getDouble("descuento"), itemJSONObject.getDouble("precio_unitario"), itemJSONObject.getDouble("valor_total"));
                    color = ((i+1)%2 == 0) ? Color.WHITE: Color.parseColor("#daeaec");

                    addTableRow(detallesPedidoTable, detallePedido, color);
                }
            }
        } catch (Exception e) {
            AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(this);
            alertExcepcion.setTitle("Error");
            alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
            alertExcepcion.setPositiveButton("OK",null);
            alertExcepcion.show();

            e.printStackTrace();
        }
    }

    public void addTableRow(TableLayout tableLayout, final DetallePedido detallePedido, int color) {
        int paddingTop = 40;
        int textSize = 15;

        TableRow tableRow = new TableRow(getApplicationContext());
        tableRow.setBackgroundColor(color);
        tableRow.setClickable(true);

        TableRow.LayoutParams trLayoutParams1;
        trLayoutParams1 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams1.weight = 2; //column weight

        TextView tv1 = new TextView(getApplicationContext());
        tv1.setPadding(0,paddingTop,0,paddingTop);
        tv1.setTextColor(Color.BLACK);
        tv1.setTextSize(textSize);
        tv1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv1.setGravity(Gravity.CENTER);
        tv1.setText(detallePedido.getProducto());
        tv1.setLayoutParams(trLayoutParams1);

        TableRow.LayoutParams trLayoutParams2;
        trLayoutParams2 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams2.weight = 1;

        TextView tv2 = new TextView(getApplicationContext());
        tv2.setPadding(0,paddingTop,0,paddingTop);
        tv2.setTextColor(Color.BLACK);
        tv2.setTextSize(textSize);
        tv2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv2.setGravity(Gravity.CENTER);
        tv2.setText("" + detallePedido.getCantidad());
        tv2.setLayoutParams(trLayoutParams2);

        TableRow.LayoutParams trLayoutParams3;
        trLayoutParams3 = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
        trLayoutParams3.weight = 1;

        TextView tv3 = new TextView(getApplicationContext());
        tv3.setPadding(0,paddingTop,0,paddingTop);
        tv3.setTextColor(Color.BLACK);
        tv3.setTextSize(textSize);
        tv3.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv3.setGravity(Gravity.CENTER);
        tv3.setText("" + detallePedido.getValor_total());
        tv3.setLayoutParams(trLayoutParams3);

        tableRow.addView(tv1);
        tableRow.addView(tv2);
        tableRow.addView(tv3);

        tableLayout.addView(tableRow);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackButton: {
                onBackPressed();
                break;
            }
            case R.id.ibvShoppingCartDetallePedido: {
                Intent activityShoppingCart = new Intent(this, CarritoPedidoActivity.class);
                startActivity(activityShoppingCart);
                break;
            }
        }
    }
}
