package com.example.administrator.ui.food;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.Utility;
import com.example.administrator.data.PedidoViewModel;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.retrofit.response.Item;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import ru.nikartm.support.ImageBadgeView;


public class MyPlatoComidaRecyclerViewAdapter extends PagedListAdapter<Item, MyPlatoComidaRecyclerViewAdapter.ViewHolder> {

    private Context mContext;

    private ImageBadgeView ibvShoppingCartPlatoRestaurant;
    PedidoViewModel pedidoViewModel;

    public MyPlatoComidaRecyclerViewAdapter(Context mContext, ImageBadgeView ibvShoppingCartPlatoRestaurant) {
        super(DIFF_CALLBACK);
        this.mContext = mContext;
        this.ibvShoppingCartPlatoRestaurant = ibvShoppingCartPlatoRestaurant;
        pedidoViewModel = ViewModelProviders.of((FragmentActivity)mContext).get(PedidoViewModel.class);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_platocomida, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = getItem(position);
        holder.tvNombre.setText(holder.mItem.getTitle());
        holder.tvPrecio.setText(holder.mItem.getPrice() + " $");

        String url_food = ClaseGlobal.getInstance().GET_FOOD_URL() + "/" + holder.mItem.getIdentifier() + "/food.jpg";
        Picasso.get()
                //.load("http://i.imgur.com/DvpvklR.png")
                .load(url_food)
                .placeholder(R.drawable.default_food)
                .error(R.drawable.default_food)
                .resize(500, 350)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                .centerCrop()
                .into(holder.ivPlatoComida);

        //holder.mView.setOnClickListener(new View.OnClickListener() {
        holder.ivPlatoComida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                String jsonItem = gson.toJson(holder.mItem);
                Intent detailProductActivity = new Intent(v.getContext(), DetalleProductoActivity.class);
                detailProductActivity.putExtra("item", jsonItem);

                v.getContext().startActivity(detailProductActivity);
            }
        });

        holder.btnCantidadProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                String jsonItem = gson.toJson(holder.mItem);
                Intent detailProductActivity = new Intent(v.getContext(), DetalleProductoActivity.class);
                detailProductActivity.putExtra("item", jsonItem);

                v.getContext().startActivity(detailProductActivity);
            }
        });

        holder.ivAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int cantidad = Integer.parseInt(Utility.obtenerCantidadProducto(holder.mItem.getIdentifier()));
                holder.btnCantidadProducto.setText(""+(cantidad + 1));

                if (cantidad == 0) {
                    pedidoViewModel.createDetallePedido(holder.mItem, "1");

                    holder.btnCantidadProducto.setVisibility(View.VISIBLE);
                } else {
                    int newCantidad = cantidad+1;
                    pedidoViewModel.updateDetallePedido(holder.mItem.getIdentifier(), "" + newCantidad);
                }

                int totalItems = Utility.getTotalItemsCart(ClaseGlobal.getInstance());
                if (totalItems == 0) {
                    ibvShoppingCartPlatoRestaurant.clearBadge();
                } else {
                    ibvShoppingCartPlatoRestaurant.setBadgeValue(totalItems);
                }
            }
        });

        String cantidadProduto = Utility.obtenerCantidadProducto(holder.mItem.getIdentifier());
        if (cantidadProduto.equals("0")) {
            holder.btnCantidadProducto.setVisibility(View.GONE);
        } else {
            holder.btnCantidadProducto.setText(cantidadProduto);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvNombre;
        public final TextView tvPrecio;
        public final ImageView ivPlatoComida;
        public final Button btnCantidadProducto;
        public final ImageView ivAddToCart;
        public Item mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvNombre = view.findViewById(R.id.tvNombre);
            tvPrecio = view.findViewById(R.id.tvPrecio);
            ivPlatoComida = view.findViewById(R.id.ivPlatoComida);
            btnCantidadProducto = view.findViewById(R.id.ivCantidadProducto);
            ivAddToCart = view.findViewById(R.id.ivAddToCart);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvNombre.getText() + "'";
        }
    }

    private static DiffUtil.ItemCallback<Item> DIFF_CALLBACK = new DiffUtil.ItemCallback<Item>() {
        @Override
        public boolean areItemsTheSame(@NonNull Item oldItem, @NonNull Item newItem) {
            return oldItem.getIdentifier().equals(newItem.getIdentifier());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Item oldItem, @NonNull Item newItem) {
            return oldItem.equals(newItem);
        }
    };

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
