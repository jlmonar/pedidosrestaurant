package com.example.administrator.domain;

public class Restaurant {
    private String codigo;
    private String nombre;
    private String dirección;

    public Restaurant(String codigo, String nombre, String dirección) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.dirección = dirección;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDirección() {
        return dirección;
    }

    public void setDirección(String dirección) {
        this.dirección = dirección;
    }
}
