package com.example.administrator.ui.restaurant;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.retrofit.response.Restaurant;
import com.example.administrator.viewmodel.RestaurantViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BusquedaRestaurantesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BusquedaRestaurantesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BusquedaRestaurantesFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerViewRestaurantes;
    private MyRestaurantRecyclerViewAdapter adaptadorRestaurant;
    RestaurantViewModel restaurantViewModel;

    private EditText etSearchRestaurant;
    private ImageButton btnSearchRestaurant;
    private TextView tvVerTodosRestaurantes;

    private OnFragmentInteractionListener mListener;

    private Context context;
    ProgressDialog progressDialog;

    public BusquedaRestaurantesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BusquedaRestaurantesFragment.
     */
    public static BusquedaRestaurantesFragment newInstance(String param1, String param2) {
        BusquedaRestaurantesFragment fragment = new BusquedaRestaurantesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_busqueda_restaurantes, container, false);

        context = getActivity().getApplicationContext();
        recyclerViewRestaurantes = view.findViewById(R.id.recyclerViewRestaurantes);

        etSearchRestaurant = view.findViewById(R.id.etSearchRestaurant);
        btnSearchRestaurant = view.findViewById(R.id.btnSearchRestaurant);
        tvVerTodosRestaurantes = view.findViewById(R.id.tvVerTodosRestaurantes);

        tvVerTodosRestaurantes.setOnClickListener(this);
        btnSearchRestaurant.setOnClickListener(this);

        recyclerViewRestaurantes.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewRestaurantes.setHasFixedSize(true);

        // Se cargan los datos desde la nube, usando paginación.
        restaurantViewModel = ViewModelProviders.of(getActivity()).get(RestaurantViewModel.class);

        adaptadorRestaurant = new MyRestaurantRecyclerViewAdapter(getActivity());

        restaurantViewModel.restaurantPagedList.observe(getActivity(), new Observer<PagedList<Restaurant>>() {
            @Override
            public void onChanged(PagedList<Restaurant> restaurants) {
                adaptadorRestaurant.submitList(restaurants);
            }
        });

        recyclerViewRestaurantes.setAdapter(adaptadorRestaurant);

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSearchRestaurant: {
                // Se esconde el teclado.
                InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                String textoBusqueda = etSearchRestaurant.getText().toString();
                if (textoBusqueda.isEmpty()) {
                    etSearchRestaurant.setError("Nombre de restaurant es requerido.");
                } else {
                    /*
                     * Remover restaurantes de busqueda previa, para ello hacemos petición al servidor usando método para buscar lineas
                     * según el texto ingresado por el usuario.
                     */
                    restaurantViewModel.setRestaurantPagedList(ClaseGlobal.GET_RESTAURANTS_BY_NAME, textoBusqueda);

                    restaurantViewModel.restaurantPagedList.observe(getActivity(), new Observer<PagedList<Restaurant>>() {
                        @Override
                        public void onChanged(PagedList<Restaurant> restaurants) {
                            adaptadorRestaurant.submitList(restaurants);

                            /* Cada vez que hacemos una busqueda desde el servidor, desactivamos este observer
                             * para que no se vuelva a lanzar cada vez que se hace una busqueda,
                             * para ello invocamos el método removeObserver.
                             */
                            restaurantViewModel.restaurantPagedList.removeObserver(this);
                        }
                    });
                }

                break;
            }
            case R.id.tvVerTodosRestaurantes: {
                /*
                 * Remover restaurantes de busqueda previa, para ello hacemos petición al servidor usando método para buscar lineas
                 * según el texto ingresado por el usuario.
                 */
                restaurantViewModel.setRestaurantPagedList(ClaseGlobal.GET_ALL_RESTAURANTS, "");

                restaurantViewModel.restaurantPagedList.observe(getActivity(), new Observer<PagedList<Restaurant>>() {
                    @Override
                    public void onChanged(PagedList<Restaurant> restaurants) {
                        adaptadorRestaurant.submitList(restaurants);

                        /* Cada vez que hacemos una busqueda desde el servidor, desactivamos este observer
                         * para que no se vuelva a lanzar cada vez que se hace una busqueda,
                         * para ello invocamos el método removeObserver.
                         */
                        restaurantViewModel.restaurantPagedList.removeObserver(this);
                    }
                });
                break;
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
