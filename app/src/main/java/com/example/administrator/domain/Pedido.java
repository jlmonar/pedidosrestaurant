package com.example.administrator.domain;

/**
 * Created by Administrator on 07/08/2018.
 */

public class Pedido {
    private String secuencia, fecha;
    private Double valorTotal;
    private String nombreVendedor, telefonoVendedor;

    public Pedido(String secuencia, String fecha, Double valorTotal, String nombreVendedor, String telefonoVendedor) {
        this.secuencia = secuencia;
        this.fecha = fecha;
        this.valorTotal = valorTotal;
        this.nombreVendedor = nombreVendedor;
        this.telefonoVendedor = telefonoVendedor;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public String getTelefonoVendedor() {
        return telefonoVendedor;
    }

    public void setTelefonoVendedor(String telefonoVendedor) {
        this.telefonoVendedor = telefonoVendedor;
    }
}
