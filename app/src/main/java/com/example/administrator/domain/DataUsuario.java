package com.example.administrator.domain;

public class DataUsuario {
    private String identificacion;
    private String nombres;
    private String apellidos;
    private String sexo;
    private String direccionDomicilio;
    private String fechaNacimiento;
    private String lugarNacimiento;
    private String email1;
    private String email2;
    private String celular1;
    private String celular2;
    private String telefono1;
    private String telefono2;
    private String telefono3;
    private String telefono4;
    private String telefono5;
    private String telefono6;

    public DataUsuario(String identificacion, String nombres, String apellidos, String sexo, String direccionDomicilio, String fechaNacimiento, String lugarNacimiento, String email1, String email2, String celular1, String celular2, String telefono1, String telefono2, String telefono3, String telefono4, String telefono5, String telefono6) {
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.direccionDomicilio = direccionDomicilio;
        this.fechaNacimiento = fechaNacimiento;
        this.lugarNacimiento = lugarNacimiento;
        this.email1 = email1;
        this.email2 = email2;
        this.celular1 = celular1;
        this.celular2 = celular2;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.telefono3 = telefono3;
        this.telefono4 = telefono4;
        this.telefono5 = telefono5;
        this.telefono6 = telefono6;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccionDomicilio() {
        return direccionDomicilio;
    }

    public void setDireccionDomicilio(String direccionDomicilio) {
        this.direccionDomicilio = direccionDomicilio;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getCelular1() {
        return celular1;
    }

    public void setCelular1(String celular1) {
        this.celular1 = celular1;
    }

    public String getCelular2() {
        return celular2;
    }

    public void setCelular2(String celular2) {
        this.celular2 = celular2;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getTelefono3() {
        return telefono3;
    }

    public void setTelefono3(String telefono3) {
        this.telefono3 = telefono3;
    }

    public String getTelefono4() {
        return telefono4;
    }

    public void setTelefono4(String telefono4) {
        this.telefono4 = telefono4;
    }

    public String getTelefono5() {
        return telefono5;
    }

    public void setTelefono5(String telefono5) {
        this.telefono5 = telefono5;
    }

    public String getTelefono6() {
        return telefono6;
    }

    public void setTelefono6(String telefono6) {
        this.telefono6 = telefono6;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNumeroTelefono() {
        if (!celular1.isEmpty())
            return celular1;
        if (!celular2.isEmpty())
            return celular2;
        if (!telefono1.isEmpty())
            return telefono1;
        if (!telefono2.isEmpty())
            return telefono2;
        if (!telefono3.isEmpty())
            return telefono3;
        if (!telefono4.isEmpty())
            return telefono4;
        if (!telefono5.isEmpty())
            return telefono5;
        if (!telefono6.isEmpty())
            return telefono6;

        return "";
    }

    public String getCorreoValido() {
        if (!email1.isEmpty())
            return email1;
        if (!email2.isEmpty())
            return email2;
        return "";
    }

    public String getCelular() {
        if (!celular1.isEmpty())
            return celular1;
        if (!celular2.isEmpty())
            return celular2;
        return "";
    }

    public String getConvencional() {
        if (!telefono1.isEmpty())
            return telefono1;
        if (!telefono2.isEmpty())
            return telefono2;
        if (!telefono3.isEmpty())
            return telefono3;
        if (!telefono4.isEmpty())
            return telefono4;
        if (!telefono5.isEmpty())
            return telefono5;
        if (!telefono6.isEmpty())
            return telefono6;

        return "";
    }
}
