package com.example.administrator.datasource.item;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.retrofit.RetrofitClient;
import com.example.administrator.retrofit.response.Item;
import com.example.administrator.retrofit.response.ResponseItems;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDataSource extends PageKeyedDataSource<Integer, Item> {
    public static final int PAGE_SIZE = 16;
    public static final int FIRST_PAGE = 1;
    private String retrofit_method = ClaseGlobal.GET_ALL_ITEMS;
    private String texto_busqueda;
    private String categoria;

    public ItemDataSource() {
    }

    public ItemDataSource(String retrofit_method, String texto_busqueda, String categoria) {
        this.retrofit_method = retrofit_method;
        this.texto_busqueda = texto_busqueda;
        this.categoria = categoria;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Item> callback) {
        switch (this.retrofit_method) {
            case ClaseGlobal.GET_ALL_ITEMS: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getAllItems(PAGE_SIZE, FIRST_PAGE)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    callback.onResult(response.body().getData(), null, FIRST_PAGE + 1);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                t.printStackTrace();
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load init).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_ITEMS_BY_NAME: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getItemsByName(texto_busqueda, PAGE_SIZE, FIRST_PAGE)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    callback.onResult(response.body().getData(), null, FIRST_PAGE + 1);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load init).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_ITEMS_BY_LINEA: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getItemsByLinea(texto_busqueda, PAGE_SIZE, FIRST_PAGE)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    callback.onResult(response.body().getData(), null, FIRST_PAGE + 1);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load init).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_ITEMS_BY_LINEA_CATEGORIA: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getItemsByLineaCategoria(texto_busqueda, categoria, PAGE_SIZE, FIRST_PAGE)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    callback.onResult(response.body().getData(), null, FIRST_PAGE + 1);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load init).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
        }
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Item> callback) {
        switch (this.retrofit_method) {
            case ClaseGlobal.GET_ALL_ITEMS: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getAllItems(PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    Integer key = (params.key > 1) ? params.key - 1 : null;
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load before).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_ITEMS_BY_NAME: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getItemsByName(texto_busqueda, PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    Integer key = (params.key > 1) ? params.key - 1 : null;
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load before).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_ITEMS_BY_LINEA: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getItemsByLinea(texto_busqueda, PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    Integer key = (params.key > 1) ? params.key - 1 : null;
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load before).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_ITEMS_BY_LINEA_CATEGORIA: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getItemsByLineaCategoria(texto_busqueda, categoria, PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    Integer key = (params.key > 1) ? params.key - 1 : null;
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load before).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
        }
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Item> callback) {
        switch (this.retrofit_method) {
            case ClaseGlobal.GET_ALL_ITEMS: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getAllItems(PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    Integer key = null;

                                    if (response.body().getMeta().getPagination().getCurrentPage() != response.body().getMeta().getPagination().getTotalPages()) {
                                        key = params.key + 1;
                                    }
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load after).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_ITEMS_BY_NAME: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getItemsByName(texto_busqueda, PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    Integer key = null;

                                    if (response.body().getMeta().getPagination().getCurrentPage() != response.body().getMeta().getPagination().getTotalPages()) {
                                        key = params.key + 1;
                                    }
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load after).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_ITEMS_BY_LINEA: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getItemsByLinea(texto_busqueda, PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    Integer key = null;

                                    if (response.body().getMeta().getPagination().getCurrentPage() != response.body().getMeta().getPagination().getTotalPages()) {
                                        key = params.key + 1;
                                    }
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load after).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
            case ClaseGlobal.GET_ITEMS_BY_LINEA_CATEGORIA: {
                RetrofitClient.getInstance()
                        .getRetrofitService()
                        .getItemsByLineaCategoria(texto_busqueda, categoria, PAGE_SIZE, params.key)
                        .enqueue(new Callback<ResponseItems>() {
                            @Override
                            public void onResponse(Call<ResponseItems> call, Response<ResponseItems> response) {
                                if (response.body() != null) {
                                    Integer key = null;

                                    if (response.body().getMeta().getPagination().getCurrentPage() != response.body().getMeta().getPagination().getTotalPages()) {
                                        key = params.key + 1;
                                    }
                                    callback.onResult(response.body().getData(), key);
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseItems> call, Throwable t) {
                                Toast.makeText(ClaseGlobal.getContext(), "Error al conectarse al servidor (load after).", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            }
        }
    }
}
