package com.example.administrator.ui.food;

import androidx.cardview.widget.CardView;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.Utility;
import com.example.administrator.data.PedidoViewModel;
import com.example.administrator.layout.CustomConstraintLayout;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.retrofit.response.Item;
import com.example.administrator.ui.cart.CarritoPedidoActivity;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import ru.nikartm.support.ImageBadgeView;

public class DetalleProductoActivity extends AppCompatActivity implements View.OnClickListener{
    private Item item;
    private ImageView ivBackbutton;
    private ImageBadgeView ibvShoppingCartDetallePedido;
    private TextView tvProductName, tvProductPrice;
    private CustomConstraintLayout layoutBackgroundPlatoComida;
    private ImageView ivMinus, ivPlus;
    private CardView cvAddToCart;
    private TextView tvQuantity;
    private TextView tvAgregarCantidad, tvValorTotal;

    private ClaseGlobal claseGlobal;

    PedidoViewModel pedidoViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_producto);

        pedidoViewModel = ViewModelProviders.of(this).get(PedidoViewModel.class);

        claseGlobal = (ClaseGlobal) getApplicationContext();

        String jsonItem = getIntent().getStringExtra("item");

        Gson gson = new Gson();
        item = gson.fromJson(jsonItem, Item.class);

        tvProductName = findViewById(R.id.tvProductName);
        tvProductPrice = findViewById(R.id.tvProductPrice);
        ivBackbutton = findViewById(R.id.ivBackButton);
        ibvShoppingCartDetallePedido = findViewById(R.id.ibvShoppingCartDetalleProducto);
        layoutBackgroundPlatoComida = findViewById(R.id.layoutBackgroundPlatoComida);

        ivMinus = findViewById(R.id.ivMinus);
        ivPlus = findViewById(R.id.ivPlus);
        cvAddToCart = findViewById(R.id.cvAddToCart);

        tvQuantity = findViewById(R.id.tvQuantity);
        tvAgregarCantidad = findViewById(R.id.tvAgregarCantidad);
        tvValorTotal = findViewById(R.id.tvValorTotal);

        String url_food = claseGlobal.GET_FOOD_URL() + "/" + item.getIdentifier() + "/food.jpg";
        Picasso.get()
                .load(url_food)
                .placeholder(R.drawable.default_food)
                .error(R.drawable.default_food)
                .resize(500, 400)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                .centerCrop()
                .into(layoutBackgroundPlatoComida);

        tvProductName.setText(item.getTitle());
        tvProductPrice.setText("$ " + String.format("%.2f", item.getPrice()));
        tvValorTotal.setText("$ " + String.format("%.2f", item.getPrice()));

        ivBackbutton.setOnClickListener(this);
        ibvShoppingCartDetallePedido.setOnClickListener(this);

        // Seteo el total de items en el carrito de compras.
        int totalItems = Utility.getTotalItemsCart(claseGlobal);
        if (totalItems == 0) {
            ibvShoppingCartDetallePedido.clearBadge();
        } else {
            ibvShoppingCartDetallePedido.setBadgeValue(totalItems);
        }

        ivMinus.setOnClickListener(this);
        ivPlus.setOnClickListener(this);
        cvAddToCart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackButton: {
                onBackPressed();
                break;
            }
            case R.id.ibvShoppingCartDetalleProducto: {
                Intent activityShoppingCart = new Intent(this, CarritoPedidoActivity.class);
                startActivity(activityShoppingCart);
                break;
            }
            case R.id.ivMinus: {
                int cantidad = Integer.parseInt(tvQuantity.getText().toString());
                cantidad = cantidad - 1;
                double valorTotal = item.getPrice() * cantidad;

                tvQuantity.setText("" + cantidad);
                tvAgregarCantidad.setText("AGREGAR " + cantidad);
                tvValorTotal.setText("$ " + String.format("%.2f", valorTotal));

                if (cantidad == 1) {
                    ivMinus.setImageResource(R.drawable.ic_remove_circle_outline_gray_24dp);
                    ivMinus.setClickable(false);
                }
                break;
            }
            case R.id.ivPlus: {
                int cantidad = Integer.parseInt(tvQuantity.getText().toString());
                cantidad = cantidad + 1;
                double valorTotal = item.getPrice() * cantidad;

                tvQuantity.setText("" + cantidad);
                tvAgregarCantidad.setText("AGREGAR " + cantidad);
                tvValorTotal.setText("$ " + String.format("%.2f", valorTotal));

                if (cantidad > 1) {
                    ivMinus.setImageResource(R.drawable.ic_remove_circle_outline_black_24dp);
                    ivMinus.setClickable(true);
                }
                break;
            }
            case R.id.cvAddToCart: {
                //Toast.makeText(this, "Preiono agregar a carrito", Toast.LENGTH_SHORT).show();
                String cantidad = tvQuantity.getText().toString();
                pedidoViewModel.createDetallePedido(item, cantidad);

                onBackPressed();
                break;
            }
        }
    }
}
