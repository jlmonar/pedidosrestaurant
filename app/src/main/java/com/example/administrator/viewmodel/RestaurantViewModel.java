package com.example.administrator.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.example.administrator.datasource.restaurant.RestaurantDataSource;
import com.example.administrator.datasource.restaurant.RestaurantDataSourceFactory;
import com.example.administrator.retrofit.response.Restaurant;

public class RestaurantViewModel extends ViewModel {
    public LiveData<PagedList<Restaurant>> restaurantPagedList;
    LiveData<PageKeyedDataSource<Integer, Restaurant>> liveDataSource;
    PagedList.Config config;

    public RestaurantViewModel() {
        RestaurantDataSourceFactory restaurantDataSourceFactory = new RestaurantDataSourceFactory();
        liveDataSource = restaurantDataSourceFactory.getRestaurantLiveData();

        config = (new PagedList.Config.Builder())
                    .setEnablePlaceholders(false)
                    .setPageSize(RestaurantDataSource.PAGE_SIZE)
                    .build();

        restaurantPagedList = (new LivePagedListBuilder(restaurantDataSourceFactory, config)).build();
    }

    /**
     * Actualizo Lista paginada de Restaurantes (Lineas)
     *
     * @param retrofit_method
     * @param texto_busqueda
     */
    public void setRestaurantPagedList(String retrofit_method, String texto_busqueda) {
        RestaurantDataSourceFactory restaurantDataSourceFactory = new RestaurantDataSourceFactory(retrofit_method, texto_busqueda);
        liveDataSource = restaurantDataSourceFactory.getRestaurantLiveData();

        restaurantPagedList = (new LivePagedListBuilder(restaurantDataSourceFactory, config)).build();
    }
}
