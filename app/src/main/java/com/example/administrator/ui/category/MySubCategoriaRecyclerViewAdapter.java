package com.example.administrator.ui.category;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.Utility;
import com.example.administrator.domain.Item;
import com.example.administrator.domain.SubCategoria;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.retrofit.response.Restaurant;
import com.example.administrator.ui.food.MyPlatoComidaRecyclerViewAdapter;
import com.example.administrator.viewmodel.ItemViewModel;


import java.util.ArrayList;
import java.util.List;


public class MySubCategoriaRecyclerViewAdapter extends RecyclerView.Adapter<MySubCategoriaRecyclerViewAdapter.ViewHolder> {

    private final List<SubCategoria> mValues;
    Context context;
    private int selectedItem;
    private RecyclerView recyclerViewPlatosComida;
    private MyPlatoComidaRecyclerViewAdapter adaptadorPlatoComidaRestaurant;
    Restaurant restaurant;
    String nivelPrecioCliente;
    ItemViewModel itemViewModel;

    public MySubCategoriaRecyclerViewAdapter(Context context, List<SubCategoria> items,
                                             RecyclerView recyclerViewPlatosComida,
                                             MyPlatoComidaRecyclerViewAdapter adaptadorPlatoComidaRestaurant,
                                             Restaurant restaurant,
                                             ItemViewModel itemViewModel) {
        this.context = context;
        this.mValues = items;
        this.recyclerViewPlatosComida = recyclerViewPlatosComida;
        this.adaptadorPlatoComidaRestaurant = adaptadorPlatoComidaRestaurant;
        this.restaurant = restaurant;
        this.itemViewModel = itemViewModel;
        selectedItem = 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_subcategoria, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.tvSubcategoryName.setText(holder.mItem.getNombre());


        holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryLight));

        if (selectedItem == position) {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }

        final List<Item> listaItems = new ArrayList<>();

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int previousItem = selectedItem;
                selectedItem = position;

                notifyItemChanged(previousItem);
                notifyItemChanged(position);

                String retrofit_method;

                if (holder.mItem.getCodigo().equals("TODOS")) {
                    retrofit_method = ClaseGlobal.GET_ITEMS_BY_LINEA;
                } else {
                    retrofit_method = ClaseGlobal.GET_ITEMS_BY_LINEA_CATEGORIA;
                }

                // Calculo el número de columnas que debe tener el grid según el ancho de la pantalla del teléfono.
                int nuberOfColumns = Utility.calculateNoOfColumns(context, 150);

                recyclerViewPlatosComida.setLayoutManager(new GridLayoutManager(context, nuberOfColumns));

                itemViewModel.setItemPagedList(retrofit_method, restaurant.getCode(), holder.mItem.getCodigo());
                itemViewModel.itemPagedList.observe((AppCompatActivity) context, new Observer<PagedList<com.example.administrator.retrofit.response.Item>>() {
                    @Override
                    public void onChanged(PagedList<com.example.administrator.retrofit.response.Item> items) {
                        adaptadorPlatoComidaRestaurant.submitList(items);

                        /* Cada vez que hacemos una busqueda desde el servidor, desactivamos este observer
                         * para que no se vuelva a lanzar cada vez que se hace una busqueda,
                         * para ello invocamos el método removeObserver.
                         */
                        itemViewModel.itemPagedList.removeObserver(this);
                    }
                });

                recyclerViewPlatosComida.setAdapter(adaptadorPlatoComidaRestaurant);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvSubcategoryName;
        public SubCategoria mItem;
        public CardView cardView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvSubcategoryName = view.findViewById(R.id.tvSubCategoryName);
            cardView = view.findViewById(R.id.cardViewSubCategory);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvSubcategoryName.getText() + "'";
        }
    }
}
