package com.example.administrator.data;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;


import com.example.administrator.retrofit.response.Item;

import java.util.HashMap;

public class PedidoViewModel extends AndroidViewModel {
    PedidoRepository pedidoRepository;
    private LiveData<HashMap<Integer, Item>> detallesPedido;

    public PedidoViewModel(@NonNull Application application) {
        super(application);
        pedidoRepository = new PedidoRepository();
        detallesPedido = pedidoRepository.getDetallesPedido();
    }

    public  LiveData<HashMap<Integer, Item>> getDetallesPedido() {
        return detallesPedido;
    }

    public void updateDetallePedido(int numero_item, String cantidad) {
        pedidoRepository.updateDetallePedido(numero_item, cantidad);
    }

    public void deleteDetallePedido(int numero_item) {
        pedidoRepository.deleteDetallePedido(numero_item);
    }

    public void createDetallePedido(Item item, String cantidad) {
        pedidoRepository.createDetallePedido(item, cantidad);
    }

    public void deleteAllDetallesPedido() {
        pedidoRepository.deleteAllDetallesPedido();
    }
}
