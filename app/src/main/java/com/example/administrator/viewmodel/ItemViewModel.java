package com.example.administrator.viewmodel;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.example.administrator.datasource.item.ItemDataSource;
import com.example.administrator.datasource.item.ItemDataSourceFactory;
import com.example.administrator.retrofit.response.Item;

public class ItemViewModel extends ViewModel {
    public LiveData<PagedList<Item>> itemPagedList;
    LiveData<PageKeyedDataSource<Integer, Item>> liveDataSource;
    PagedList.Config config;

    public ItemViewModel() {
        ItemDataSourceFactory itemDataSourceFactory = new ItemDataSourceFactory();
        liveDataSource = itemDataSourceFactory.getItemLiveData();

        config = (new PagedList.Config.Builder())
                .setEnablePlaceholders(false)
                .setPageSize(ItemDataSource.PAGE_SIZE)
                .build();

        itemPagedList = (new LivePagedListBuilder(itemDataSourceFactory, config)).build();
    }

    /**
     * Cambia lista paginada según el requiermimiento enviado al servidor y el texto de búsqueda.
     *
     * @param retrofit_method
     * @param texto_busqueda
     */
    public void setItemPagedList(String retrofit_method, String texto_busqueda, String categoria) {
        ItemDataSourceFactory itemDataSourceFactory = new ItemDataSourceFactory(retrofit_method, texto_busqueda, categoria);
        liveDataSource = itemDataSourceFactory.getItemLiveData();

        itemPagedList = (new LivePagedListBuilder(itemDataSourceFactory, config)).build();
    }
}
