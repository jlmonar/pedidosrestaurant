package com.example.administrator.ui.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.administrator.request.MyCustomRequest;
import com.google.android.material.textfield.TextInputEditText;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.domain.Client;
import com.example.administrator.pedidosapp.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PerfilUsuarioFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PerfilUsuarioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PerfilUsuarioFragment extends Fragment implements View.OnClickListener {
    private ClaseGlobal claseGlobal;
    private Context context;
    private SharedPreference sharedPreferences;
    private Client cliente;
    private TextInputEditText etNombres, etApellido, etTelefono, etCorreo;
    private TextView tvIdentificacion;
    private Button btnGuardarCambios;
    private ProgressDialog progressDialog;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PerfilUsuarioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PerfilUsuarioFragment.
     */
    public static PerfilUsuarioFragment newInstance(String param1, String param2) {
        PerfilUsuarioFragment fragment = new PerfilUsuarioFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        context = getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil_usuario, container, false);

        claseGlobal = (ClaseGlobal) context;
        sharedPreferences = new SharedPreference();

        etNombres = view.findViewById(R.id.etNombresUsuario);
        etApellido = view.findViewById(R.id.etApellidosUsuario);
        etTelefono = view.findViewById(R.id.etTelefonoUsuario);
        etCorreo = view.findViewById(R.id.etCorreoUsuario);
        tvIdentificacion = view.findViewById(R.id.tvIdentificacionUsuario);
        btnGuardarCambios = view.findViewById(R.id.btnGuardarCambios);

        String jsonClientPref = sharedPreferences.getValue(context, "client", "");
        Gson gson = new Gson();
        cliente = gson.fromJson(jsonClientPref, Client.class);

        tvIdentificacion.setText(cliente.getCodigo());
        etNombres.setText(cliente.getNombre());
        etApellido.setText(cliente.getApellido());
        etCorreo.setText(cliente.getCorreo());
        etTelefono.setText(cliente.getTelefono());

        btnGuardarCambios.setOnClickListener(this);

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGuardarCambios: {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Aztualizando datos...");

                Client clienteActualizado = new Client(
                        cliente.getCodigo(),
                        cliente.getRuc(),
                        etNombres.getText().toString(),
                        etApellido.getText().toString(),
                        cliente.getTipo(),
                        cliente.getDescuento(),
                        etCorreo.getText().toString(),
                        etTelefono.getText().toString(),
                        cliente.getDireccionDomicilio()
                );

                Gson gson = new Gson();
                final String jsonClienteActualizado = gson.toJson(clienteActualizado);

                Map<String, String> params = new HashMap<>();
                params.put("cliente_registro", jsonClienteActualizado);

                String urlActualizarDatosCliente = ClaseGlobal.getInstance().GET_METHOD_RESOURCE_CLIENTES() + "/" + cliente.getCodigo();

                // Tag used to cancel the request
                String  tag_string_req = "string_req";

                MyCustomRequest putRequest = new MyCustomRequest(Request.Method.PUT, urlActualizarDatosCliente, context,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response.toString());
                                    String success = jsonObject.getString("data");

                                    if (success.equals("true")) {
                                        // Guardamos nuevos datos del cliente aztualizado en SharedPreferences.
                                        sharedPreferences.saveString(context,"client", jsonClienteActualizado);

                                        AlertDialog.Builder alertError = new AlertDialog.Builder(getActivity());
                                        alertError.setTitle("Mensaje");
                                        alertError.setMessage("Usuario actualizado con éxito.");
                                        alertError.setPositiveButton("OK", null);
                                        alertError.show();
                                    }
                                } catch (JSONException e) {
                                    AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(getActivity());
                                    alertExcepcion.setTitle("Error");
                                    alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                                    alertExcepcion.setPositiveButton("OK",null);
                                    alertExcepcion.show();

                                    e.printStackTrace();
                                }
                                progressDialog.dismiss();
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("RESPONSE API ERROR", "Error: " + error.getMessage());
                        AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(getActivity());
                        alertErrorResponse.setTitle("Error");
                        alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                        alertErrorResponse.setPositiveButton("OK",null);
                        alertErrorResponse.show();
                        progressDialog.dismiss();
                    }
                })  {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("cliente_registro", jsonClienteActualizado);

                        return params;
                    }
                };
                // Adding request to request queue
                ClaseGlobal.getInstance().addToRequestQueue(putRequest, tag_string_req);

                break;
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
