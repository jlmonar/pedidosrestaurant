package com.example.administrator.data;

import androidx.lifecycle.MutableLiveData;

import android.util.Log;
import android.widget.Toast;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.retrofit.response.Item;
import com.google.gson.Gson;

import java.util.HashMap;

public class PedidoRepository {
    MutableLiveData<HashMap<Integer,Item>> detallesPedido;
    SharedPreference sharedPreferences;

    public PedidoRepository() {
        detallesPedido = getDetallesPedido();
        sharedPreferences = new SharedPreference();
    }

    public MutableLiveData<HashMap<Integer, Item>> getDetallesPedido() {
        if (detallesPedido == null) {
            detallesPedido = new MutableLiveData<>();
        }

        HashMap<Integer,Item> mapItems = ClaseGlobal.getInstance().getMapItems();
        detallesPedido.setValue(mapItems);

        return detallesPedido;
    }

    public void updateDetallePedido(int numero_item, String cantidad) {
        HashMap<Integer, Item> mapItems = ClaseGlobal.getInstance().getMapItems();

        if (cantidad.equals("0")) {
            mapItems.remove(numero_item);
        } else {
            Item itemedited = mapItems.get(numero_item);
            mapItems.put(itemedited.getIdentifier(),
                    new Item(
                            itemedited.getIdentifier(),
                            itemedited.getCodigo(),
                            itemedited.getTitle(),
                            itemedited.getStock(),
                            itemedited.getPrecioVenta1(),
                            itemedited.getPrecioVenta2(),
                            itemedited.getPrecioVenta3(),
                            itemedited.getPrecioVenta4(),
                            itemedited.getPrecioVenta5(),
                            itemedited.getPrecioVenta6(),
                            itemedited.getPrecioVenta7(),
                            itemedited.getPrecioVenta8(),
                            itemedited.getPrecioFOB(),
                            itemedited.getPrecioDVenta1(),
                            itemedited.getPrecioDVenta2(),
                            itemedited.getPrecioDVenta3(),
                            itemedited.getPrecioDVenta4(),
                            itemedited.getPrecioDVenta5(),
                            itemedited.getPrecioDVenta6(),
                            itemedited.getPrecioDVenta7(),
                            itemedited.getPrecioDVenta8(),
                            cantidad
                    )
            );
        }

        detallesPedido.setValue(mapItems);

        //Update sharedPreferences
        Gson gson = new Gson();
        String jsonHashMapItems = gson.toJson(mapItems);
        sharedPreferences.saveString(ClaseGlobal.getContext(),"items",jsonHashMapItems);
    }

    public void deleteDetallePedido(int numero_item) {
        HashMap<Integer,Item> mapItems = ClaseGlobal.getInstance().getMapItems();
        mapItems.remove(numero_item);

        detallesPedido.setValue(mapItems);

        //Update sharedPreferences
        Gson gson = new Gson();
        String jsonHashMapItems = gson.toJson(mapItems);
        sharedPreferences.saveString(ClaseGlobal.getContext(),"items",jsonHashMapItems);
    }

    public void createDetallePedido(Item item, String cantidad) {

        HashMap<Integer,Item> mapItems = ClaseGlobal.getInstance().getMapItems();
        Item itemFind = mapItems.get(item.getIdentifier());

        if (itemFind == null) {
            item.setQuantity(cantidad);
            mapItems.put(item.getIdentifier(), item);
        } else {
            int newQuantity = Integer.parseInt(itemFind.getQuantity()) + Integer.parseInt(cantidad);
            itemFind.setQuantity("" + newQuantity);
            mapItems.put(itemFind.getIdentifier(),itemFind);
        }

        detallesPedido.setValue(mapItems);

        //Refresh items sharedPreferences
        Gson gson = new Gson();
        String jsonMapItems = gson.toJson(mapItems);
        sharedPreferences.saveString(ClaseGlobal.getContext(), "items", jsonMapItems);

        Toast.makeText(ClaseGlobal.getContext(), "Producto agregado al carrito.", Toast.LENGTH_SHORT).show();
    }

    public void deleteAllDetallesPedido() {
        HashMap<Integer,Item> mapItems = new HashMap<Integer, Item>();

        ClaseGlobal claseGlobal = ClaseGlobal.getInstance();
        claseGlobal.setMapItems(mapItems);

        sharedPreferences.removeValue(ClaseGlobal.getContext(),"items");

        detallesPedido.setValue(mapItems);
    }
}
