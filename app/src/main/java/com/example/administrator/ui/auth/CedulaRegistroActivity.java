package com.example.administrator.ui.auth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.Utility;
import com.example.administrator.domain.DataUsuario;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.request.MyCustomRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CedulaRegistroActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnSiguiente;
    private EditText etCedulaRegistro;
    private ImageView ivBackButton;

    private ProgressDialog progressDialog;

    private DataUsuario datausuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cedula_registro);

        btnSiguiente = findViewById(R.id.btnSiguiente);
        etCedulaRegistro = findViewById(R.id.etCedulaRegistro);
        ivBackButton = findViewById(R.id.ivBackButton);

        ivBackButton.setOnClickListener(this);
        btnSiguiente.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSiguiente: {
                //Se esconde el teclado
                InputMethodManager inputMethodManager =(InputMethodManager)this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);

                if (etCedulaRegistro.getText().length() == 0) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle("Error en Identificación.");
                    alert.setMessage("Escriba un numero de cédula antes de continuar.");
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    return;
                }

                if (!Utility.VerificaIdentificacion(etCedulaRegistro.getText().toString())) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle("Error en Identificación.");
                    alert.setMessage("La identificación ingresada no es válida.");
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    return;
                }

                /**
                 * Aquí debo chequear primero si la cedula ingresada ya está registrada en ARCLIENTES,
                 * si no lo está, entonces se obtiene la data del usuario usando el apidatarestful.
                 */
                progressDialog = new ProgressDialog(this);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Consultando...");

                String url = ClaseGlobal.getInstance().GET_METHOD_EXISTE_CLIENTE() + etCedulaRegistro.getText();
                // Tag used to cancel the request
                String  tag_string_req = "string_req";
                final Context context = this;

                Log.i("ACCESS_TAG", "VA A ENTRAR A GET Verificar existencia cliente");
                StringRequest strReq = new StringRequest(Request.Method.GET,
                        url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i("ACCESS_TAG", "Entró a GET Verificar existencia cliente");

                            JSONObject jsonObject = new JSONObject(response.toString());
                            String existeClienteDataJson = jsonObject.getString("data");

                            //JSONObject jsonObject = new JSONObject(response.toString());
                            //JSONObject existeClienteDataJson = jsonObject.getJSONObject("data");

                            if (existeClienteDataJson.equals("false")) {
                                /**
                                 * Antes de ir al nuevo activity, hay que obtener del usuario con el apidatamaster.
                                 */
                                String urlGetToken = ClaseGlobal.getInstance().GET_TOKEN_URL();
                                String tag_json_obj = "json_obj_req";

                                Log.i("ACCESS_TAG", "VA A ENTRAR a obtener TOKEN");

                                StringRequest postRequest = new StringRequest(Request.Method.POST, urlGetToken,
                                        new Response.Listener<String>()
                                        {
                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    JSONObject jsonObject = new JSONObject(response.toString());
                                                    final String access_token = jsonObject.getString("access_token");

                                                    Log.i("ACCESS_TAG", access_token);
                                                    /////////////////////////////////

                                                    //String url_get_data_usuario = ClaseGlobal.getInstance().GET_USUARIO_URL() + "0100000934";
                                                    String url_get_data_usuario = ClaseGlobal.getInstance().GET_USUARIO_URL() + etCedulaRegistro.getText();

                                                    // Tag used to cancel the request
                                                    String  tag_string_req = "string_req";

                                                    Log.i("ACCESS_TAG", "VA A ENTRAR a obtener DATA USUARIO");
                                                    MyCustomRequest strReq = new MyCustomRequest(Request.Method.GET,
                                                            url_get_data_usuario, context, new Response.Listener<String>() {

                                                        @Override
                                                        public void onResponse(String response) {
                                                            try {
                                                                JSONObject jsonObject = new JSONObject(response.toString());
                                                                JSONObject dataUsuarioJson = jsonObject.getJSONObject("data");
                                                                //String dataUsuarioJson = jsonObject.getString("data");

                                                                datausuario = new DataUsuario(
                                                                        dataUsuarioJson.getString("identificacion"),
                                                                        dataUsuarioJson.getString("nombres"),
                                                                        dataUsuarioJson.getString("nombres"),
                                                                        dataUsuarioJson.getString("sexo"),
                                                                        dataUsuarioJson.getString("domicilio"),
                                                                        dataUsuarioJson.getString("fecha_nacimiento"),
                                                                        dataUsuarioJson.getString("lugar_nacimiento"),
                                                                        dataUsuarioJson.getString("email_1"),
                                                                        dataUsuarioJson.getString("email_2"),
                                                                        dataUsuarioJson.getString("celular_1"),
                                                                        dataUsuarioJson.getString("celular_2"),
                                                                        dataUsuarioJson.getString("telefono_1"),
                                                                        dataUsuarioJson.getString("telefono_2"),
                                                                        dataUsuarioJson.getString("telefono_3"),
                                                                        dataUsuarioJson.getString("telefono_4"),
                                                                        dataUsuarioJson.getString("telefono_5"),
                                                                        dataUsuarioJson.getString("telefono_6")
                                                                );

                                                                Gson gson = new Gson();
                                                                String jsonDataUsuario = gson.toJson(datausuario);
                                                                Log.i("ACCESS_TAG", dataUsuarioJson.toString());

                                                                Intent intentRegistroUsuario = new Intent(context, RegistroUsuarioActivity.class);
                                                                intentRegistroUsuario.putExtra("data_usuario_json", jsonDataUsuario);
                                                                startActivity(intentRegistroUsuario);
                                                            } catch (JSONException e) {
                                                                AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(context);
                                                                alertExcepcion.setTitle("Error JSONException");
                                                                alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                                                                alertExcepcion.setPositiveButton("OK",null);
                                                                alertExcepcion.show();

                                                                e.printStackTrace();
                                                            }

                                                            progressDialog.dismiss();
                                                        }
                                                    }, new Response.ErrorListener() {

                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            Log.d("RESPONSE API ERROR", "Error: " + error.getMessage());
                                                            //ESTÁ ENTRANDO AQUÍ CUANDO EL JSON TIENE RESPUESTA DE ERROR.
                                                            /*
                                                            AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                                                            alertErrorResponse.setTitle("OnErrorResponse");
                                                            alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                                                            alertErrorResponse.setPositiveButton("OK",null);
                                                            alertErrorResponse.show();
                                                            */
                                                            Intent intentRegistroUsuario = new Intent(context, RegistroUsuarioActivity.class);
                                                            intentRegistroUsuario.putExtra("cedula_usuario", etCedulaRegistro.getText().toString());
                                                            startActivity(intentRegistroUsuario);
                                                            progressDialog.dismiss();
                                                        }
                                                    }) {
                                                        @Override
                                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                                            Map<String, String>  params = new HashMap<String, String>();
                                                            params.put("Authorization", "Bearer " + access_token);
                                                            //params.put("Accept-Language", "fr");

                                                            return params;
                                                        }
                                                    };
                                                    // Adding request to request queue
                                                    ClaseGlobal.getInstance().addToRequestQueue(strReq, tag_string_req);

                                                    //Intent intentRegistroUsuario = new Intent(context, RegistroUsuarioActivity.class);
                                                    //startActivity(intentRegistroUsuario);

                                                    /////////////////////////////////
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                    AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                                    alertError.setTitle("Error");
                                                    alertError.setMessage("Error insperado. Contacte a soporte. EXCEPCION");
                                                    alertError.setPositiveButton("OK",null);
                                                    alertError.show();
                                                }
                                                //progressDialog.dismiss();
                                            }
                                        },
                                        new Response.ErrorListener()
                                        {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                // error
                                                AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                                                alertErrorResponse.setTitle("Error");
                                                alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                                                alertErrorResponse.setPositiveButton("OK",null);
                                                alertErrorResponse.show();
                                                progressDialog.dismiss();
                                            }
                                        }
                                ) {
                                    @Override
                                    protected Map<String, String> getParams()
                                    {
                                        Map<String, String>  params = new HashMap<>();
                                        params.put("grant_type", "password");
                                        params.put("client_id", "2");
                                        params.put("client_secret", "Lv902Mx6TqZhB9THClSFu0pcrVOVG2iwiXXdxfzH");
                                        params.put("username", "joseph@mail.com");
                                        params.put("password", "joseph123");
                                        return params;
                                    }
                                };
                                postRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                // Adding request to request queue
                                ClaseGlobal.getInstance().addToRequestQueue(postRequest, tag_json_obj);

                            } else {
                                //String errorMessage = jsonObject.getString("error");

                                AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                alertError.setTitle("Usuario ya Registrado");
                                alertError.setMessage("Ya se encuentra usuario registrado con la cedula/ruc: " + etCedulaRegistro.getText());
                                alertError.setPositiveButton("OK",null);
                                alertError.show();
                                progressDialog.dismiss();
                            }
                        } catch (JSONException e) {
                            AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(context);
                            alertExcepcion.setTitle("Error");
                            alertExcepcion.setMessage("Error insperado. Contacte a soporte. Excepción: " + e.getMessage());
                            alertExcepcion.setPositiveButton("OK",null);
                            alertExcepcion.show();

                            e.printStackTrace();
                        }
                        //progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("ACCESS_TAG", "voley error.");
                        AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                        alertErrorResponse.setTitle("Error");
                        alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                        alertErrorResponse.setPositiveButton("OK",null);
                        alertErrorResponse.show();
                        progressDialog.dismiss();
                    }
                });
                // Adding request to request queue
                ClaseGlobal.getInstance().addToRequestQueue(strReq, tag_string_req);


                break;
            }
            case R.id.ivBackButton: {
                onBackPressed();
                break;
            }
        }
    }
}
