package com.example.administrator.ui.restaurant;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.layout.CustomConstraintLayout;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.retrofit.response.Restaurant;
import com.google.gson.Gson;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;



public class MyRestaurantRecyclerViewAdapter extends PagedListAdapter<Restaurant, MyRestaurantRecyclerViewAdapter.ViewHolder> {

    private Context mContext;

    public MyRestaurantRecyclerViewAdapter(Context mContext) {
        super(DIFF_CALLBACK);
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_restaurant, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = getItem(position);
        holder.tvNombreRestaurant.setText(holder.mItem.getName());

        String url_restaurant = ClaseGlobal.getInstance().GET_RESTAURANTES_URL() + "/" + holder.mItem.getCode() + "/restaurant.jpg";
        Picasso.get()
                .load(url_restaurant)
                .placeholder(R.drawable.default_restaurant)
                .error(R.drawable.default_restaurant)
                .resize(500, 350)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                .centerCrop()
                .into(holder.layoutBackground);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                String jsonRestaurant = gson.toJson(holder.mItem);
                Intent platosComidaRestaurantActivity = new Intent(v.getContext(), PlatosPorRestaurantActivity.class);
                platosComidaRestaurantActivity.putExtra("restaurant", jsonRestaurant);

                v.getContext().startActivity(platosComidaRestaurantActivity);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public CustomConstraintLayout layoutBackground = null;
        public final TextView tvNombreRestaurant;
        public Restaurant mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            layoutBackground = view.findViewById(R.id.layoutBackgroundImage);
            tvNombreRestaurant = view.findViewById(R.id.tvNombreRestaurant);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvNombreRestaurant.getText() + "'";
        }
    }

    private static DiffUtil.ItemCallback<Restaurant> DIFF_CALLBACK = new DiffUtil.ItemCallback<Restaurant>() {
        @Override
        public boolean areItemsTheSame(@NonNull Restaurant oldItem, @NonNull Restaurant newItem) {
            return oldItem.getCode().equals(newItem.getCode());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Restaurant oldItem, @NonNull Restaurant newItem) {
            return oldItem.equals(newItem);
        }
    };
}
