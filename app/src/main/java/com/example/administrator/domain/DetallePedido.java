package com.example.administrator.domain;

/**
 * Created by Administrator on 09/08/2018.
 */

public class DetallePedido {
    private String linea, producto;
    private int cantidad;
    private double descuento, precio_unitario,valor_total;

    public DetallePedido(String linea, String producto, int cantidad, double descuento, double precio_unitario, double valor_total) {
        this.linea = linea;
        this.producto = producto;
        this.cantidad = cantidad;
        this.descuento = descuento;
        this.precio_unitario = precio_unitario;
        this.valor_total = valor_total;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(double precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public double getValor_total() {
        return valor_total;
    }

    public void setValor_total(double valor_total) {
        this.valor_total = valor_total;
    }
}
