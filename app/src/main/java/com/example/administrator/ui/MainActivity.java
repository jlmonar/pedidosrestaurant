package com.example.administrator.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import com.example.administrator.config.Utility;
import com.example.administrator.retrofit.response.Item;
import com.example.administrator.ui.cart.CarritoPedidoActivity;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.domain.Client;
import com.example.administrator.pedidosapp.R;
import com.example.administrator.ui.food.BusquedaItemsFragment;
import com.example.administrator.ui.restaurant.BusquedaRestaurantesFragment;
import com.example.administrator.ui.auth.LoginActivity;
import com.example.administrator.ui.order.HistorialPedidosFragment;
import com.example.administrator.ui.profile.PerfilUsuarioFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;

import ru.nikartm.support.ImageBadgeView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BusquedaItemsFragment.OnFragmentInteractionListener,
        HistorialPedidosFragment.OnFragmentInteractionListener,
        PerfilUsuarioFragment.OnFragmentInteractionListener,
        BusquedaRestaurantesFragment.OnFragmentInteractionListener, View.OnClickListener {

    private ClaseGlobal claseGlobal;
    private SharedPreference sharedPreferences;
    private TextView tvClientName, tvNombreEmpresa, tvEmpresaDelivery;
    private String empresa;
    private ImageBadgeView ibvShoppingCart;

    final Fragment perfilUsuarioFragment = new PerfilUsuarioFragment();
    final Fragment busquedaRestaurantesFragment = new BusquedaRestaurantesFragment();
    final Fragment busquedaItemsFragment = new BusquedaItemsFragment();
    final Fragment historialPedidosFragment = new HistorialPedidosFragment();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = busquedaRestaurantesFragment;

    NavigationView mNavigationView;
    View mHeaderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarMain);

        // NavigationView
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        // NavigationView Header
        mHeaderView =  mNavigationView.getHeaderView(0);

        tvClientName = mHeaderView.findViewById(R.id.tvNombreCliente);
        tvNombreEmpresa = mHeaderView.findViewById(R.id.tvNombreEmpresa);

        tvEmpresaDelivery =  toolbar.findViewById(R.id.tvEmpresaDelivery);
        ibvShoppingCart = toolbar.findViewById(R.id.ibvShoppingCart);
        ibvShoppingCart.setOnClickListener(this);

        claseGlobal = (ClaseGlobal) getApplicationContext();

        sharedPreferences = new SharedPreference();
        Gson gson = new Gson();

        String jsonClientPref = sharedPreferences.getValue(this, "client", "");
        String jsonItemsPref = sharedPreferences.getValue(this, "items", "");

        if (jsonClientPref.isEmpty()) {
            //There is not a logged user.
            Intent loginActivity = new Intent(this, LoginActivity.class);
            startActivity(loginActivity);
            finish();
            return;
        }

        empresa = sharedPreferences.getValue(this, "empresa", "");
        tvNombreEmpresa.setText(empresa);
        tvEmpresaDelivery.setText(empresa);

        Client clientFromJSON = gson.fromJson(jsonClientPref, Client.class);

        tvClientName.setText(clientFromJSON.getNombre() + " " + clientFromJSON.getApellido());

        if (!jsonItemsPref.isEmpty()) {
            //Shopping cart have elements.
            //From stringJson to HashMap.
            Type typeMap = new TypeToken<HashMap<Integer, Item>>(){}.getType();
            HashMap<Integer,Item> itemsHashMapFromJSON = gson.fromJson(jsonItemsPref, typeMap);

            claseGlobal.setMapItems(itemsHashMapFromJSON);
        } else {
            //Shopping cart is empty.
            claseGlobal.setMapItems(new HashMap<Integer, Item>());
        }

        // Seteo el total de items en el carrito de compras.
        int totalItems = Utility.getTotalItemsCart(claseGlobal);
        if (totalItems == 0) {
            ibvShoppingCart.clearBadge();
        } else {
            ibvShoppingCart.setBadgeValue(totalItems);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        //Se selecciona el primer item del NavigationDrawer.
        navigationView.getMenu().getItem(1).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(1));

        navigationView.setNavigationItemSelectedListener(this);

        fm.beginTransaction().add(R.id.content_main, perfilUsuarioFragment, "1").hide(perfilUsuarioFragment).commit();
        fm.beginTransaction().add(R.id.content_main, busquedaItemsFragment, "3").hide(busquedaItemsFragment).commit();
        fm.beginTransaction().add(R.id.content_main, historialPedidosFragment, "4").hide(historialPedidosFragment).commit();
        fm.beginTransaction().add(R.id.content_main, busquedaRestaurantesFragment, "2").commit();

        fixGoogleMapBug();   //TODO: Then clean this line
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        boolean fragmentTransaction = false;

        if (id == R.id.nav_profile) {
            fm.beginTransaction().hide(active).show(perfilUsuarioFragment).commit();
            active = perfilUsuarioFragment;
            fragmentTransaction = true;
        } else if (id == R.id.nav_restaurant) {
            fm.beginTransaction().hide(active).show(busquedaRestaurantesFragment).commit();
            active = busquedaRestaurantesFragment;
            fragmentTransaction = true;
        } else if (id == R.id.nav_food) {
            fm.beginTransaction().hide(active).show(busquedaItemsFragment).commit();
            active = busquedaItemsFragment;
            fragmentTransaction = true;
        } else if (id == R.id.nav_history) {
            fm.beginTransaction().hide(active).show(historialPedidosFragment).commit();
            active = historialPedidosFragment;
            fragmentTransaction = true;
        } else if (id == R.id.nav_logout) {
            sharedPreferences.removeValue(this,"client");
            sharedPreferences.removeValue(this,"items");
            sharedPreferences.removeValue(this,"nivelPrecio");
            sharedPreferences.removeValue(this,"coordenada_destino");
            Intent loginActivity = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(loginActivity);
            finish();
        }

        if (fragmentTransaction) {
            item.setChecked(true);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * Observation: Invoke this method only once
     */
    private void fixGoogleMapBug() {
        SharedPreferences googleBug = getSharedPreferences("google_bug", Context.MODE_PRIVATE);
        if (!googleBug.contains("fixed")) {
            File corruptedZoomTables = new File(getFilesDir(), "ZoomTables.data");
            corruptedZoomTables.delete();
            googleBug.edit().putBoolean("fixed", true).apply();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibvShoppingCart: {
                Intent activityShoppingCart = new Intent(this, CarritoPedidoActivity.class);
                startActivity(activityShoppingCart);
                break;
            }
        }
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();

        int totalItems = Utility.getTotalItemsCart(claseGlobal);
        if (totalItems == 0) {
            ibvShoppingCart.clearBadge();
        } else {
            ibvShoppingCart.setBadgeValue(totalItems);
        }
    }
}
