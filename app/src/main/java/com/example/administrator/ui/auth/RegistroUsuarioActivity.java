package com.example.administrator.ui.auth;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.example.administrator.domain.Client;
import com.example.administrator.domain.DataUsuario;
import com.example.administrator.request.MyCustomRequest;
import com.example.administrator.retrofit.response.Item;
import com.example.administrator.ui.MainActivity;
import com.example.administrator.pedidosapp.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegistroUsuarioActivity extends AppCompatActivity implements View.OnClickListener {
    private DataUsuario dataUsuario;
    private TextView tvIdentificacionRegistro;
    private EditText etNombres, etApellidos, etTelefono, etEmail, etDomicilio;
    private Button btnRegistrarUsuario;
    private ImageView ivBackButton;

    private ProgressDialog progressDialog;

    private ClaseGlobal claseGlobal;
    private Context context;
    private SharedPreference sharedPreferences;

    private String IMEI = "";

    private static final String READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE;
    private static final int READ_PHONE_PERMISSION_REQUEST_CODE = 1234;
    private Boolean mReadPhonePermissionsGranted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        context = this;
        claseGlobal = (ClaseGlobal) getApplicationContext();

        tvIdentificacionRegistro = findViewById(R.id.tvIdentificacionRegistro);
        etNombres = findViewById(R.id.etNombresUsuario);
        etApellidos = findViewById(R.id.etApellidosUsuario);
        etDomicilio = findViewById(R.id.etDireccionDomicilio);
        etTelefono = findViewById(R.id.etTelefonoUsuario);
        etEmail = findViewById(R.id.etCorreoUsuario);
        ivBackButton = findViewById(R.id.ivBackButton);

        btnRegistrarUsuario = findViewById(R.id.btnRegistrarUsuario);

        sharedPreferences = new SharedPreference();

        String jsonDataUsuario = getIntent().getStringExtra("data_usuario_json");

        if (jsonDataUsuario != null) {
            Gson gson = new Gson();
            dataUsuario = gson.fromJson(jsonDataUsuario, DataUsuario.class);

            tvIdentificacionRegistro.setText(dataUsuario.getIdentificacion());
            etNombres.setText(dataUsuario.getNombres());
            etApellidos.setText(dataUsuario.getApellidos());
            etDomicilio.setText(dataUsuario.getDireccionDomicilio());
            etTelefono.setText(dataUsuario.getNumeroTelefono());
            etEmail.setText(dataUsuario.getCorreoValido());
        } else {
            String cedulaUsuario = getIntent().getStringExtra("cedula_usuario");
            tvIdentificacionRegistro.setText(cedulaUsuario);
        }

        btnRegistrarUsuario.setOnClickListener(this);
        ivBackButton.setOnClickListener(this);

        getReadPhonePermission();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegistrarUsuario: {
                boolean camposTextosLlenos = true;
                // Agrego validaciones
                if (etNombres.getText().toString().isEmpty()) {
                    etNombres.setError("Nombre del cliente es requerido.");
                    camposTextosLlenos = false;
                }

                if (etApellidos.getText().toString().isEmpty()) {
                    etApellidos.setError("Apellido del cliente es requerido.");
                    camposTextosLlenos = false;
                }

                if (etEmail.getText().toString().isEmpty()) {
                    etEmail.setError("Correo electrónico es requerido.");
                    camposTextosLlenos = false;
                }

                if (etDomicilio.getText().toString().isEmpty()) {
                    etDomicilio.setError("Dirección de domicilio es requerido.");
                    camposTextosLlenos = false;
                }

                if (etTelefono.getText().toString().isEmpty()) {
                    etTelefono.setError("Teléfono es requerido.");
                    camposTextosLlenos = false;
                }

                if (!camposTextosLlenos) {
                    return;
                }

                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Registrando Usuario...");

                String rucId = "0";
                if (tvIdentificacionRegistro.getText().toString().length() == 13) {
                    rucId = tvIdentificacionRegistro.getText().toString();
                }
                //Registrar el usuario.
                final Client clienteRegistro = new Client(
                        tvIdentificacionRegistro.getText().toString(),
                        rucId,
                        etNombres.getText().toString(),
                        etApellidos.getText().toString(),
                        "1",
                        0.0,
                        etEmail.getText().toString(),
                        etTelefono.getText().toString(),
                        etDomicilio.getText().toString()
                );
                Gson gson = new Gson();
                final String jsonCliente = gson.toJson(clienteRegistro);

                TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (mReadPhonePermissionsGranted) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    IMEI = mngr.getDeviceId();
                }

                String urlResourceCliente = ClaseGlobal.getInstance().GET_METHOD_RESOURCE_CLIENTES();
                String tag_json_obj = "json_obj_req";

                MyCustomRequest postRequest = new MyCustomRequest(Request.Method.POST, urlResourceCliente, context,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response.toString());
                                    String success = jsonObject.getString("data");

                                    if (success.equals("true")) {
                                        btnRegistrarUsuario.setEnabled(false);

                                        claseGlobal.setMapItems(new HashMap<Integer, Item>());
                                        AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                        alertError.setTitle("Mensaje");
                                        alertError.setMessage("Usuario registrado con éxito.");
                                        alertError.setPositiveButton("OK", null);
                                        alertError.show();

                                        progressDialog.setMessage("Obteniendo información...");

                                        // DEBO GUARDAR DATOS EN EL SHARED_PREFERENCES E IR A LA PANTALLA INICIAL.
                                        //CAMBIOS 05/06/2019
                                        sharedPreferences.saveString(context,"client", jsonCliente);

                                        String urlNivelPrecio = ClaseGlobal.getInstance().GET_METHOD_FIND_TIPO_CLIENTE() + clienteRegistro.getCodigo();

                                        // Tag used to cancel the request
                                        String  tag_string_req_nivel_precio = "string_req";

                                        MyCustomRequest strReqNivelPrecio = new MyCustomRequest(Request.Method.GET,
                                                urlNivelPrecio, context, new Response.Listener<String>() {

                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    JSONObject jsonObject = new JSONObject(response.toString());

                                                    JSONObject tipoClienteDataJson = jsonObject.getJSONObject("data");

                                                    if (tipoClienteDataJson != null) {
                                                        sharedPreferences.saveString(context,"nivelPrecio", tipoClienteDataJson.getString("priceLevel"));

                                                        String urlParametros = ClaseGlobal.getInstance().GET_METHOD_PARAMETROS();

                                                        // Tag used to cancel the request
                                                        String  tag_string_req_parametros = "string_req";

                                                        MyCustomRequest strReqParametros = new MyCustomRequest(Request.Method.GET,
                                                                urlParametros, context, new Response.Listener<String>() {

                                                            @Override
                                                            public void onResponse(String response) {
                                                                try {
                                                                    JSONObject jsonObject = new JSONObject(response.toString());
                                                                    JSONArray dataJSONArray = jsonObject.getJSONArray("data");

                                                                    if (dataJSONArray.length() > 0) {
                                                                        for ( int i=0; i < dataJSONArray.length(); i++) {
                                                                            JSONObject itemJSONObject = dataJSONArray.getJSONObject(i);

                                                                            String id = itemJSONObject.getString("id");
                                                                            if (id.equals("1")) {
                                                                                sharedPreferences.saveString(context,"desglosaIva", itemJSONObject.getString("value"));
                                                                            } else if(id.equals("2")) {
                                                                                sharedPreferences.saveString(context,"empresa", itemJSONObject.getString("value"));
                                                                            }
                                                                        }

                                                                        Intent mainActivity = new Intent(context, MainActivity.class);
                                                                        startActivity(mainActivity);
                                                                    } else {
                                                                        AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                                                        alertError.setTitle("Sin resultados");
                                                                        alertError.setMessage("No se encontraron resultados.");
                                                                        alertError.setPositiveButton("OK",null);
                                                                        alertError.show();
                                                                    }
                                                                } catch (JSONException e) {
                                                                    AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(context);
                                                                    alertExcepcion.setTitle("Error");
                                                                    alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                                                                    alertExcepcion.setPositiveButton("OK",null);
                                                                    alertExcepcion.show();

                                                                    e.printStackTrace();
                                                                }
                                                                progressDialog.dismiss();
                                                            }
                                                        }, new Response.ErrorListener() {

                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {
                                                                AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                                                                alertErrorResponse.setTitle("Error");
                                                                alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                                                                alertErrorResponse.setPositiveButton("OK",null);
                                                                alertErrorResponse.show();
                                                                progressDialog.dismiss();
                                                            }
                                                        });
                                                        // Adding request to request queue
                                                        ClaseGlobal.getInstance().addToRequestQueue(strReqParametros, tag_string_req_parametros);
                                                    } else {
                                                        String errorMessage = jsonObject.getString("error");

                                                        AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                                        alertError.setTitle("Sin resultados");
                                                        alertError.setMessage("No se encontraron resultados. ERROR: " + errorMessage);
                                                        alertError.setPositiveButton("OK",null);
                                                        alertError.show();
                                                    }
                                                } catch (JSONException e) {
                                                    AlertDialog.Builder alertExcepcion = new AlertDialog.Builder(context);
                                                    alertExcepcion.setTitle("Error");
                                                    alertExcepcion.setMessage("Error insperado. Contacte a soporte. " + e.getMessage());
                                                    alertExcepcion.setPositiveButton("OK",null);
                                                    alertExcepcion.show();

                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {

                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                                                alertErrorResponse.setTitle("Error");
                                                alertErrorResponse.setMessage("Error insperado. Contacte a soporte. " + error.getMessage());
                                                alertErrorResponse.setPositiveButton("OK",null);
                                                alertErrorResponse.show();
                                                progressDialog.dismiss();
                                            }
                                        });
                                        // Adding request to request queue
                                        ClaseGlobal.getInstance().addToRequestQueue(strReqNivelPrecio, tag_string_req_nivel_precio);
                                    } else {
                                        AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                        alertError.setTitle("Error");
                                        alertError.setMessage("Error al registrar usuario. Contacte a soporte. ");
                                        alertError.setPositiveButton("OK", null);
                                        alertError.show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    AlertDialog.Builder alertError = new AlertDialog.Builder(context);
                                    alertError.setTitle("Error");
                                    alertError.setMessage("Error al registrar usuario. Contacte a soporte. EXCEPCION");
                                    alertError.setPositiveButton("OK", null);
                                    alertError.show();
                                    progressDialog.dismiss();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                AlertDialog.Builder alertErrorResponse = new AlertDialog.Builder(context);
                                alertErrorResponse.setTitle("Error");
                                alertErrorResponse.setMessage("Error al registrar usuario. Contacte a soporte. " + error.getMessage());
                                alertErrorResponse.setPositiveButton("OK", null);
                                alertErrorResponse.show();
                                progressDialog.dismiss();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("cliente_registro", jsonCliente);
                        params.put("imei", IMEI);

                        if (dataUsuario != null) {
                            Gson gson = new Gson();
                            String jsonDataUsuario = gson.toJson(dataUsuario);

                            params.put("data_usuario", jsonDataUsuario);
                        }

                        return params;
                    }
                };
                postRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                // Adding request to request queue
                ClaseGlobal.getInstance().addToRequestQueue(postRequest, tag_json_obj);
                break;
            }
            case R.id.ivBackButton: {
                onBackPressed();
                break;
            }
        }
    }

    private void getReadPhonePermission() {
        String[] permissions = {Manifest.permission.READ_PHONE_STATE};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {

            mReadPhonePermissionsGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    permissions,
                    READ_PHONE_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Log.d(TAG, "onRequestPermissionsResult: called.");
        mReadPhonePermissionsGranted = false;

        switch (requestCode) {
            case READ_PHONE_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mReadPhonePermissionsGranted = false;
                            //Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    //Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mReadPhonePermissionsGranted = true;
                }
            }
        }
    }
}
