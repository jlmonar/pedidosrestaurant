package com.example.administrator.config;



import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.administrator.retrofit.response.Item;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by José Luis on 13/03/2018.
 */

public class ClaseGlobal  extends Application {
    public static final String TAG = ClaseGlobal.class.getSimpleName();
    public static final String API_KEY_GOOGLE_MAPS = "AIzaSyBgTJw-2dxkHFd2wOAK-SzFXccqkhpN0F4";

    private HashMap<Integer,Item> mapItems;
    private RequestQueue mRequestQueue;

    private static ClaseGlobal mInstance;

    //Api URL
    //private String API_URL = "http://192.168.0.4:8013/apirestfulpedidosrestaurant/public/api";
    //private String API_URL = "http://192.168.100.20:8013/apirestfulpedidosrestaurant/public/api";
    private String API_URL = "http://corporacionsmartest.com/apirestfulpedidosrestaurant/public/api";

    // Rutas para el api que me permite obtener información de personas.
    //private String API_GET_TOKEN_URL = "http://192.168.0.4:8013/apirestfuldatamasterrestaurant/public/oauth/token";
    //private String API_GET_USUARIO_URL = "http://192.168.0.4:8013/apirestfuldatamasterrestaurant/public/api/datausuarios/";
    private String API_GET_TOKEN_URL = "http://corporacionsmartest.com/apirestfuldatamasterrestaurant/public/oauth/token";
    private String API_GET_USUARIO_URL = "http://corporacionsmartest.com/apirestfuldatamasterrestaurant/public/api/datausuarios/";

    // Ruta de bucket de imágenes de platos de comida.
    private String FOOD_URL = "http://corporacionsmartest.com/pedidos_app/platos_comida";

    // Ruta de bucket de imágenes de restaurantes.
    private String RESTAURANT_URL = "http://corporacionsmartest.com/pedidos_app/restaurantes";

    //Methods
    private String METHOD_SEARCH_ITEMS_BY_NAME = GET_API_URL() + "/items/searchitems/";
    private String METHOD_FIND_CLIENT_BY_ID = GET_API_URL() + "/clientes/";
    private String METHOD_RESOURCE_CLIENTES = GET_API_URL() + "/clientes";
    private String METHOD_FIND_TIPO_CLIENTE = GET_API_URL() + "/tipos_clientes/";
    private String METHOD_INSERT_PEDIDO = GET_API_URL() + "/pedido";
    private String METHOD_GET_PEDIDOS_BY_DATE = GET_API_URL() + "/pedido/getbydate/";
    private String METHOD_GET_DETALLES_PEDIDOS = GET_API_URL() + "/detalles_pedido/";
    private String METHOD_GET_PARAMETROS = GET_API_URL() + "/parametros";
    private String METHOD_EXISTE_CLIENTE = GET_API_URL() + "/existe_cliente/";
    private String METHOD_FIND_PLATO_BY_LINEA = GET_API_URL() + "/items/findbylinea/";
    private String METHOD_FIND_PLATO_BY_LINEA_CATEGORIA = GET_API_URL() + "/items/findbylineacategoria/";
    private String METHOD_RESOURCE_RESTAURANTES = GET_API_URL() + "/restaurantes";
    private String METHOD_FIND_RESTAURANTS_BY_NAME = GET_API_URL() + "/restaurantes/findbyname/";
    private String METHOD_GET_SUBCATEGORIAS = GET_API_URL() + "/restaurantes/getsubcategorias/";

    // OPCIONES PARA LLEVAR.
    public static final String PEDIDO_LLEVAR = "1";
    public static final String PEDIDO_DOMICILIO = "2";

    //Methods Retrofit.
    public static final String GET_ALL_RESTAURANTS = "GET_ALL_RESTAURANTS";
    public static final String GET_RESTAURANTS_BY_NAME = "GET_RESTAURANTS_BY_NAME";
    public static final String GET_ALL_ITEMS = "GET_ALL_ITEMS";
    public static final String GET_ITEMS_BY_NAME = "GET_ITEMS_BY_NAME";
    public static final String GET_ITEMS_BY_LINEA = "GET_ITEMS_BY_LINEA";
    public static final String GET_ITEMS_BY_LINEA_CATEGORIA = "GET_ITEMS_BY_LINEA_CATEGORIA";

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized ClaseGlobal getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public HashMap<Integer, Item> getMapItems() {
        return mapItems;
    }

    public void setMapItems(HashMap<Integer, Item> mapItems) {
        this.mapItems = mapItems;
    }

    public String GET_API_URL() {
        return API_URL;
    }

    public String GET_FOOD_URL() {
        return FOOD_URL;
    }

    public String GET_RESTAURANTES_URL() {
        return RESTAURANT_URL;
    }

    public String GET_METHOD_SEARCH_ITEMS_BY_NAME() {
        return METHOD_SEARCH_ITEMS_BY_NAME;
    }

    public String GET_METHOD_RESOURCE_CLIENTES() {
        return METHOD_RESOURCE_CLIENTES;
    }

    public String GET_METHOD_FIND_CLIENT_BY_ID() {
        return METHOD_FIND_CLIENT_BY_ID;
    }

    public String GET_METHOD_FIND_TIPO_CLIENTE() {
        return METHOD_FIND_TIPO_CLIENTE;
    }

    public String GET_METHOD_INSERT_PEDIDO() {
        return METHOD_INSERT_PEDIDO;
    }

    public String GET_METHOD_GET_PEDIDOS_BY_DATE() {
        return METHOD_GET_PEDIDOS_BY_DATE;
    }

    public String GET_METHOD_DETALLES_PEDIDO() {
        return METHOD_GET_DETALLES_PEDIDOS;
    }

    public String GET_METHOD_PARAMETROS() {
        return METHOD_GET_PARAMETROS;
    }

    public String GET_METHOD_EXISTE_CLIENTE() {
        return METHOD_EXISTE_CLIENTE;
    }

    public String GET_TOKEN_URL() {
        return API_GET_TOKEN_URL;
    }

    public String GET_USUARIO_URL() {
        return API_GET_USUARIO_URL;
    }

    public String GET_METHOD_FIND_PLATO_BY_LINEA() {
        return METHOD_FIND_PLATO_BY_LINEA;
    }

    public String GET_METHOD_FIND_PLATO_BY_LINEA_CATEGORIA() {
        return METHOD_FIND_PLATO_BY_LINEA_CATEGORIA;
    }

    public String GET_METHOD_SUBCATEGORIAS() {
        return METHOD_GET_SUBCATEGORIAS;
    }

    public String GET_METHOD_RESOURCE_RESTAURANTES() {
        return METHOD_RESOURCE_RESTAURANTES;
    }

    public String GET_METHOD_FIND_RESTAURANTS_BY_NAME() {
        return METHOD_FIND_RESTAURANTS_BY_NAME;
    }

    public Double obtenerPrecioVentaSegunTipoCliente(String tipoCliente, JSONObject itemJSONObject) {
        try {
            switch (tipoCliente) {
                case "1": {
                    return itemJSONObject.getDouble("precioDVenta1");
                }
                case "2": {
                    return itemJSONObject.getDouble("precioDVenta2");
                }
                case "3": {
                    return itemJSONObject.getDouble("precioDVenta3");
                }
                case "4": {
                    return itemJSONObject.getDouble("precioDVenta4");
                }
                case "5": {
                    return itemJSONObject.getDouble("precioDVenta5");
                }
                case "6": {
                    return itemJSONObject.getDouble("precioDVenta6");
                }
                case "7": {
                    return itemJSONObject.getDouble("precioDVenta7");
                }
                case "8": {
                    return itemJSONObject.getDouble("precioDVenta8");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Context getContext() {
        return  mInstance;
    }
}
