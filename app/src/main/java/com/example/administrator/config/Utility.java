package com.example.administrator.config;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;

import com.example.administrator.retrofit.response.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Utility {
    public static int calculateNoOfColumns(Context context, float columnWidthDp) { // For example columnWidthdp=180
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;

        return (int) (screenWidthDp / columnWidthDp);
    }

    public static int getTotalItemsCart(ClaseGlobal claseGlobal) {
        HashMap<Integer, Item> mapItems = claseGlobal.getMapItems();
        List<Item> itemsList = new ArrayList<>(mapItems.values());
        int totalItems = 0;

        for (int i=0; i < itemsList.size(); i++) {
            totalItems = totalItems + Integer.parseInt(itemsList.get(i).getQuantity());
        }

        return totalItems;
    }

    public static boolean VerificaIdentificacion(String identificacion)
    {
        boolean estado = false;
        char[] valced = new char[13];
        int provincia;
        if (identificacion.length() >= 10 && containsOnlyNumbers(identificacion))
        {
            valced = identificacion.trim().toCharArray();
            provincia = Integer.parseInt(("" + valced[0] + valced[1]));

            if (provincia > 0 && provincia < 25)
            {
                if (Integer.parseInt("" + valced[2]) < 6 && valced.length == 10)
                {
                    estado = VerificaCedula(valced);
                }
                else if (Integer.parseInt("" + valced[2]) < 6 && valced.length == 13)
                {
                    estado = VerificaRucNatural(valced);
                }
                else if (Integer.parseInt("" + valced[2]) == 6)
                {
                    estado = VerificaRucPublico(valced);
                }
                else if (Integer.parseInt("" + valced[2]) == 9)
                {
                    estado = VerificaRucJuridico(valced);
                }
            }
        }
        return estado;
    }

    public static boolean containsOnlyNumbers(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i)))
                return false;
        }
        return true;
    }

    public static boolean VerificaCedula(char[] validarCedula)
    {
        int aux = 0, par = 0, impar = 0, verifi;
        for (int i = 0; i < 9; i += 2)
        {
            aux = 2 * Integer.parseInt("" + validarCedula[i]);
            if (aux > 9)
                aux -= 9;
            par += aux;
        }
        for (int i = 1; i < 9; i += 2)
        {
            impar += Integer.parseInt("" + validarCedula[i]);
        }

        aux = par + impar;
        if (aux % 10 != 0)
        {
            verifi = 10 - (aux % 10);
        }
        else
            verifi = 0;
        if (verifi == Integer.parseInt("" + validarCedula[9]))
            return true;
        else
            return false;
    }


    public static boolean VerificaRucNatural(char[] validarCedula)
    {
        int veri;

        if (validarCedula.length != 13) {
            return false;
        }

        // Para RUCS Naturales tercer digito debe ser menor a 6.
        if (Integer.parseInt("" + validarCedula[2]) < 0 || Integer.parseInt("" + validarCedula[2]) > 5)
        {
            return false;
        }

        veri = Integer.parseInt("" + validarCedula[10]) + Integer.parseInt("" + validarCedula[11]) + Integer.parseInt("" + validarCedula[12]);
        if (veri < 1)
        {
            return false;
        }

        return VerificaCedula(validarCedula);
    }

    public static boolean VerificaRucJuridico(char[] validarCedula)
    {
        int aux = 0, prod, veri;

        if (validarCedula.length != 13) {
            return false;
        }

        // Para RUCS Jurídicos tercer digito debe ser 9.
        if (Integer.parseInt("" + validarCedula[2]) != 9) {
            return false;
        }

        veri = Integer.parseInt("" + validarCedula[10]) + Integer.parseInt("" + validarCedula[11]) + Integer.parseInt("" + validarCedula[12]);
        if (veri > 0)
        {
            int[] coeficiente = new int[] { 4, 3, 2, 7, 6, 5, 4, 3, 2 };
            for (int i = 0; i < 9; i++)
            {
                prod = Integer.parseInt("" + validarCedula[i]) * coeficiente[i];
                aux += prod;
            }
            if (aux % 11 == 0)
            {
                veri = 0;
            }
            else if (aux % 11 == 1)
            {
                return false;
            }
            else
            {
                aux = aux % 11;
                veri = 11 - aux;
            }

            if (veri == Integer.parseInt("" + validarCedula[9]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static boolean VerificaRucPublico(char[] validarCedula)
    {
        int aux = 0, prod, veri;

        if (validarCedula.length != 13) {
            return false;
        }

        // Para RUCS Públicos tercer digito debe ser 6.
        if (Integer.parseInt("" + validarCedula[2]) != 6) {
            return false;
        }

        veri = Integer.parseInt("" + validarCedula[9]) + Integer.parseInt("" + validarCedula[10]) + Integer.parseInt("" + validarCedula[11]) + Integer.parseInt("" + validarCedula[12]);
        if (veri > 0)
        {
            int[] coeficiente = new int[] { 3, 2, 7, 6, 5, 4, 3, 2 };

            for (int i = 0; i < 8; i++)
            {
                prod = Integer.parseInt("" + validarCedula[i]) * coeficiente[i];
                aux += prod;
            }

            if (aux % 11 == 0)
            {
                veri = 0;
            }
            else if (aux % 11 == 1)
            {
                return false;
            }
            else
            {
                aux = aux % 11;
                veri = 11 - aux;
            }

            if (veri == Integer.parseInt("" + validarCedula[8]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static String obtenerCantidadProducto(int identifier) {
        ClaseGlobal claseGlobal = ClaseGlobal.getInstance();

        HashMap<Integer,Item> mapItems = ClaseGlobal.getInstance().getMapItems();
        Item itemFind = mapItems.get(identifier);

        if (itemFind == null) {
            return "0";
        } else {
            return itemFind.getQuantity();
        }
    }
}
