
package com.example.administrator.retrofit.response;

import com.example.administrator.config.ClaseGlobal;
import com.example.administrator.config.SharedPreference;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("identifier")
    @Expose
    private Integer identifier;
    @SerializedName("codigo")
    @Expose
    private String codigo;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("stock")
    @Expose
    private Double stock;
    @SerializedName("precioVenta1")
    @Expose
    private Double precioVenta1;
    @SerializedName("precioVenta2")
    @Expose
    private Double precioVenta2;
    @SerializedName("precioVenta3")
    @Expose
    private Double precioVenta3;
    @SerializedName("precioVenta4")
    @Expose
    private Double precioVenta4;
    @SerializedName("precioVenta5")
    @Expose
    private Double precioVenta5;
    @SerializedName("precioVenta6")
    @Expose
    private Double precioVenta6;
    @SerializedName("precioVenta7")
    @Expose
    private Double precioVenta7;
    @SerializedName("precioVenta8")
    @Expose
    private Double precioVenta8;
    @SerializedName("precioFOB")
    @Expose
    private Double precioFOB;
    @SerializedName("precioDVenta1")
    @Expose
    private Double precioDVenta1;
    @SerializedName("precioDVenta2")
    @Expose
    private Double precioDVenta2;
    @SerializedName("precioDVenta3")
    @Expose
    private Double precioDVenta3;
    @SerializedName("precioDVenta4")
    @Expose
    private Double precioDVenta4;
    @SerializedName("precioDVenta5")
    @Expose
    private Double precioDVenta5;
    @SerializedName("precioDVenta6")
    @Expose
    private Double precioDVenta6;
    @SerializedName("precioDVenta7")
    @Expose
    private Double precioDVenta7;
    @SerializedName("precioDVenta8")
    @Expose
    private Double precioDVenta8;

    private String quantity;
    private Double price;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Item() {
    }

    /**
     * 
     * @param identifier
     * @param precioVenta8
     * @param codigo
     * @param precioVenta7
     * @param precioVenta6
     * @param precioFOB
     * @param precioDVenta8
     * @param precioDVenta7
     * @param precioDVenta6
     * @param precioDVenta5
     * @param precioDVenta4
     * @param precioDVenta3
     * @param precioDVenta2
     * @param title
     * @param precioDVenta1
     * @param precioVenta1
     * @param stock
     * @param precioVenta5
     * @param precioVenta4
     * @param precioVenta3
     * @param precioVenta2
     */
    public Item(Integer identifier, String codigo, String title, Double stock, Double precioVenta1, Double precioVenta2, Double precioVenta3, Double precioVenta4, Double precioVenta5, Double precioVenta6, Double precioVenta7, Double precioVenta8, Double precioFOB, Double precioDVenta1, Double precioDVenta2, Double precioDVenta3, Double precioDVenta4, Double precioDVenta5, Double precioDVenta6, Double precioDVenta7, Double precioDVenta8) {
        super();
        this.identifier = identifier;
        this.codigo = codigo;
        this.title = title;
        this.stock = stock;
        this.precioVenta1 = precioVenta1;
        this.precioVenta2 = precioVenta2;
        this.precioVenta3 = precioVenta3;
        this.precioVenta4 = precioVenta4;
        this.precioVenta5 = precioVenta5;
        this.precioVenta6 = precioVenta6;
        this.precioVenta7 = precioVenta7;
        this.precioVenta8 = precioVenta8;
        this.precioFOB = precioFOB;
        this.precioDVenta1 = precioDVenta1;
        this.precioDVenta2 = precioDVenta2;
        this.precioDVenta3 = precioDVenta3;
        this.precioDVenta4 = precioDVenta4;
        this.precioDVenta5 = precioDVenta5;
        this.precioDVenta6 = precioDVenta6;
        this.precioDVenta7 = precioDVenta7;
        this.precioDVenta8 = precioDVenta8;
    }

    /**
     * Constructor que incluye la cantidad elegida por el usuario.
     *
     * @param identifier
     * @param codigo
     * @param title
     * @param stock
     * @param precioVenta1
     * @param precioVenta2
     * @param precioVenta3
     * @param precioVenta4
     * @param precioVenta5
     * @param precioVenta6
     * @param precioVenta7
     * @param precioVenta8
     * @param precioFOB
     * @param precioDVenta1
     * @param precioDVenta2
     * @param precioDVenta3
     * @param precioDVenta4
     * @param precioDVenta5
     * @param precioDVenta6
     * @param precioDVenta7
     * @param precioDVenta8
     * @param quantity
     */
    public Item(Integer identifier, String codigo, String title, Double stock, Double precioVenta1, Double precioVenta2, Double precioVenta3, Double precioVenta4, Double precioVenta5, Double precioVenta6, Double precioVenta7, Double precioVenta8, Double precioFOB, Double precioDVenta1, Double precioDVenta2, Double precioDVenta3, Double precioDVenta4, Double precioDVenta5, Double precioDVenta6, Double precioDVenta7, Double precioDVenta8, String quantity) {
        super();
        this.identifier = identifier;
        this.codigo = codigo;
        this.title = title;
        this.stock = stock;
        this.precioVenta1 = precioVenta1;
        this.precioVenta2 = precioVenta2;
        this.precioVenta3 = precioVenta3;
        this.precioVenta4 = precioVenta4;
        this.precioVenta5 = precioVenta5;
        this.precioVenta6 = precioVenta6;
        this.precioVenta7 = precioVenta7;
        this.precioVenta8 = precioVenta8;
        this.precioFOB = precioFOB;
        this.precioDVenta1 = precioDVenta1;
        this.precioDVenta2 = precioDVenta2;
        this.precioDVenta3 = precioDVenta3;
        this.precioDVenta4 = precioDVenta4;
        this.precioDVenta5 = precioDVenta5;
        this.precioDVenta6 = precioDVenta6;
        this.precioDVenta7 = precioDVenta7;
        this.precioDVenta8 = precioDVenta8;
        this.quantity = quantity;
    }

    public Integer getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Integer identifier) {
        this.identifier = identifier;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getStock() {
        return stock;
    }

    public void setStock(Double stock) {
        this.stock = stock;
    }

    public Double getPrecioVenta1() {
        return precioVenta1;
    }

    public void setPrecioVenta1(Double precioVenta1) {
        this.precioVenta1 = precioVenta1;
    }

    public Double getPrecioVenta2() {
        return precioVenta2;
    }

    public void setPrecioVenta2(Double precioVenta2) {
        this.precioVenta2 = precioVenta2;
    }

    public Double getPrecioVenta3() {
        return precioVenta3;
    }

    public void setPrecioVenta3(Double precioVenta3) {
        this.precioVenta3 = precioVenta3;
    }

    public Double getPrecioVenta4() {
        return precioVenta4;
    }

    public void setPrecioVenta4(Double precioVenta4) {
        this.precioVenta4 = precioVenta4;
    }

    public Double getPrecioVenta5() {
        return precioVenta5;
    }

    public void setPrecioVenta5(Double precioVenta5) {
        this.precioVenta5 = precioVenta5;
    }

    public Double getPrecioVenta6() {
        return precioVenta6;
    }

    public void setPrecioVenta6(Double precioVenta6) {
        this.precioVenta6 = precioVenta6;
    }

    public Double getPrecioVenta7() {
        return precioVenta7;
    }

    public void setPrecioVenta7(Double precioVenta7) {
        this.precioVenta7 = precioVenta7;
    }

    public Double getPrecioVenta8() {
        return precioVenta8;
    }

    public void setPrecioVenta8(Double precioVenta8) {
        this.precioVenta8 = precioVenta8;
    }

    public Double getPrecioFOB() {
        return precioFOB;
    }

    public void setPrecioFOB(Double precioFOB) {
        this.precioFOB = precioFOB;
    }

    public Double getPrecioDVenta1() {
        return precioDVenta1;
    }

    public void setPrecioDVenta1(Double precioDVenta1) {
        this.precioDVenta1 = precioDVenta1;
    }

    public Double getPrecioDVenta2() {
        return precioDVenta2;
    }

    public void setPrecioDVenta2(Double precioDVenta2) {
        this.precioDVenta2 = precioDVenta2;
    }

    public Double getPrecioDVenta3() {
        return precioDVenta3;
    }

    public void setPrecioDVenta3(Double precioDVenta3) {
        this.precioDVenta3 = precioDVenta3;
    }

    public Double getPrecioDVenta4() {
        return precioDVenta4;
    }

    public void setPrecioDVenta4(Double precioDVenta4) {
        this.precioDVenta4 = precioDVenta4;
    }

    public Double getPrecioDVenta5() {
        return precioDVenta5;
    }

    public void setPrecioDVenta5(Double precioDVenta5) {
        this.precioDVenta5 = precioDVenta5;
    }

    public Double getPrecioDVenta6() {
        return precioDVenta6;
    }

    public void setPrecioDVenta6(Double precioDVenta6) {
        this.precioDVenta6 = precioDVenta6;
    }

    public Double getPrecioDVenta7() {
        return precioDVenta7;
    }

    public void setPrecioDVenta7(Double precioDVenta7) {
        this.precioDVenta7 = precioDVenta7;
    }

    public Double getPrecioDVenta8() {
        return precioDVenta8;
    }

    public void setPrecioDVenta8(Double precioDVenta8) {
        this.precioDVenta8 = precioDVenta8;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        SharedPreference sharedPreferences = new SharedPreference();

        String nivelPrecioCliente = sharedPreferences.getValue(ClaseGlobal.getContext(), "nivelPrecio", "");

        switch (nivelPrecioCliente) {
            case "1": {
                return precioDVenta1;
            }
            case "2": {
                return precioDVenta2;
            }
            case "3": {
                return precioDVenta3;
            }
            case "4": {
                return precioDVenta4;
            }
            case "5": {
                return precioDVenta5;
            }
            case "6": {
                return precioDVenta6;
            }
            case "7": {
                return precioDVenta7;
            }
            case "8": {
                return precioDVenta8;
            }
        }


        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
