
package com.example.administrator.retrofit.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseRestaurants {

    @SerializedName("data")
    @Expose
    private List<Restaurant> data = null;
    @SerializedName("meta")
    @Expose
    private Meta meta;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ResponseRestaurants() {
    }

    /**
     * 
     * @param data
     * @param meta
     */
    public ResponseRestaurants(List<Restaurant> data, Meta meta) {
        super();
        this.data = data;
        this.meta = meta;
    }

    public List<Restaurant> getData() {
        return data;
    }

    public void setData(List<Restaurant> data) {
        this.data = data;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

}
