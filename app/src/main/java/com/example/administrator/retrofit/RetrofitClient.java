package com.example.administrator.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    //private static final String BASE_URL = "http://192.168.100.20:8013/apirestfulpedidosrestaurant/public/api/";
    private static final String BASE_URL = "http://corporacionsmartest.com/apirestfulpedidosrestaurant/public/api/";
    private static RetrofitClient mInstance;
    private RetrofitService retrofitService;
    private Retrofit retrofit;

    private RetrofitClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofitService = retrofit.create(RetrofitService.class);
    }

    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }

        return mInstance;
    }

    public RetrofitService getRetrofitService() {
        return retrofitService;
    }
}
