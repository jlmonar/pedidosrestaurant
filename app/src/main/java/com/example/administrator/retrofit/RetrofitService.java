package com.example.administrator.retrofit;

import com.example.administrator.retrofit.response.ResponseItems;
import com.example.administrator.retrofit.response.ResponseRestaurants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {
    @GET("restaurantes")
    Call<ResponseRestaurants> getAllRestaurants(@Query("per_page") int per_page, @Query("page") int page);

    @GET("restaurantes/findbyname/{restaurant_name}")
    Call<ResponseRestaurants> getRestaurantsByName(@Path("restaurant_name") String restaurant_name, @Query("per_page") int per_page, @Query("page") int page);

    @GET("items")
    Call<ResponseItems> getAllItems(@Query("per_page") int per_page, @Query("page") int page);

    @GET("items/searchitems/{item_name}")
    Call<ResponseItems> getItemsByName(@Path("item_name") String item_name, @Query("per_page") int per_page, @Query("page") int page);

    @GET("items/findbylinea/{linea}")
    Call<ResponseItems> getItemsByLinea(@Path("linea") String linea, @Query("per_page") int per_page, @Query("page") int page);

    @GET("items/findbylineacategoria/{linea}/{categoria}")
    Call<ResponseItems> getItemsByLineaCategoria(@Path("linea") String linea, @Path("categoria") String categoria, @Query("per_page") int per_page, @Query("page") int page);
}
